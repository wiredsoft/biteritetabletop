package com.krapps.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.smokeysdinein.R;

import java.util.HashMap;


//import com.bugsense.trace.BugSenseHandler;

public class BaseActivity extends AppCompatActivity implements onUpdateViewListener, OnClickListener {

    private static AlertDialog alertDialog;
    private BaseApplication mApplication;
    private long delayTimeForExitToast = 2000;
    private long backPressedTime;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;

    private boolean isWindowFocused;
    private boolean isAppWentToBg;
    private boolean isBackPressed;
    private static int runningActivities = 0;
    private int imgSize;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        mApplication = (BaseApplication) getApplication();

    }

    public void clearAllFragment() {
        FragmentManager fm = this.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    /*public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack) {
        findViewById(R.id.content_frame).setVisibility(View.VISIBLE);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(R.id.content_frame, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(R.id.content_frame, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }*/


    public void showProgressDialog() {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage("Loading");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mApplication.activityResumed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mApplication.activityPaused();
    }

    /**
     * hides the soft key pad
     */
    public void hideSoftKeypad(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(BaseActivity.class.getSimpleName(), "hideSoftKeypad()", e);
        }
    }

    public void loadUrlOnBrowser(String url) {
        try {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception ex) {
            ToastUtils.showToast(this, "Url is not proper!");
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                // ToastUtils.showToast(this,
                // getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {

                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {


            default:
                url = "";
                className = null;
                break;
        }

        VolleyStringRequest request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
        }, getParams(reqType));
        ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();


        return params;
    }

    public void onStop() {
        super.onStop();


    }

    public enum HEADER_MODE {
        MENU_ONLY, MENU_WITH_SEARCH, BACK_WITH_SEARCH, BACK_WITHOUT_SEARCH, BACK_WITH_FORWARD, ONLY_FORWARD, MENU_WITH_SETTING, BOTH_SIDE_TEXT, SEARCH;
    }
}
