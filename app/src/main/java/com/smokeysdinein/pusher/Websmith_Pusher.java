package com.smokeysdinein.pusher;

import android.os.AsyncTask;
import android.util.Log;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vineet.Rajpoot on 9/7/2015.
 */
public class Websmith_Pusher implements ChannelEventListener, ConnectionEventListener {

    private static final ScheduledExecutorService connectionAttemptsWorker = Executors.newSingleThreadScheduledExecutor();
    public static ConnectionState targetState = ConnectionState.CONNECTED;
    private static int MAX_RETRIES = 10;
    private static String PUBLIC_CHANNEL_NAME;
    private static String PUBLIC_EVENT_NAME;
    private static int failedConnectionAttempts;
    private static Pusher pusher;

    static {
        failedConnectionAttempts = 0;
    }

    private Channel publicChannel;

    public Websmith_Pusher() {
        PUBLIC_CHANNEL_NAME = "3000050";
        PUBLIC_EVENT_NAME = "3000050";
        targetState = ConnectionState.CONNECTED;
        HttpAuthorizer localHttpAuthorizer = new HttpAuthorizer("http://www.leggetter.co.uk/pusher/pusher-examples/php/authentication/src/private_auth.php");
        pusher = new Pusher("55d5f9b3215c887945c0", new PusherOptions().setEncrypted(true).setAuthorizer(localHttpAuthorizer));
        Log.d("STATE", "" + pusher.getConnection().getState());
        pusher.getConnection().bind(ConnectionState.ALL, this);
        publicChannel = pusher.subscribe(PUBLIC_CHANNEL_NAME, this, new String[]{PUBLIC_EVENT_NAME});
        achieveExpectedConnectionState();
    }

    public static void achieveExpectedConnectionState() {
        Object localObject = pusher.getConnection().getState();
        if (localObject == targetState) {
            failedConnectionAttempts = 0;
        }
        do {
            if ((targetState == ConnectionState.CONNECTED) && (failedConnectionAttempts == MAX_RETRIES)) {
                targetState = ConnectionState.DISCONNECTED;
                log("failed to connect after " + failedConnectionAttempts + " attempts. Reconnection attempts stopped.");
                return;
            }
            if ((localObject == ConnectionState.DISCONNECTED) && (targetState == ConnectionState.CONNECTED)) {
                localObject = new Runnable() {
                    public void run() {
                        Websmith_Pusher.pusher.connect();
                    }
                };
                log("Connecting in " + failedConnectionAttempts + " seconds");
                connectionAttemptsWorker.schedule((Runnable) localObject, failedConnectionAttempts, TimeUnit.SECONDS);
                failedConnectionAttempts += 1;
                return;
            }
        }
        while ((localObject != ConnectionState.CONNECTED) || (targetState != ConnectionState.DISCONNECTED));
        pusher.unsubscribe(PUBLIC_CHANNEL_NAME);
        pusher.disconnect();
    }

    public static void disconect() {
        pusher.disconnect();
    }

    private static void generateNotification(String paramString) {
        long l = System.currentTimeMillis();
        /*NotificationManager localNotificationManager = (NotificationManager) Login_Screen.context.getSystemService("notification");
        Notification localNotification = new Notification(2130837618, paramString, l);
        String str = Login_Screen.context.getString(2131361810);
        Object localObject = new Intent(Login_Screen.context, Home_Screen.class);
        ((Intent) localObject).setFlags(872415232);
        localObject = PendingIntent.getActivity(Login_Screen.context, 0, (Intent) localObject, 0);
        localNotification.setLatestEventInfo(Login_Screen.context, str, paramString, (PendingIntent) localObject);
        localNotification.flags |= 0x10;
        localNotification.sound = RingtoneManager.getDefaultUri(2);
        localNotification.defaults |= 0x2;
        localNotificationManager.notify(0, localNotification);*/
    }

    private static void log(String paramString) {
        new LogTask(paramString).execute(new Void[0]);
    }

    public void onConnectionStateChange(ConnectionStateChange paramConnectionStateChange) {
        String.format("Connection state changed from [%s] to [%s]", new Object[]{paramConnectionStateChange.getPreviousState(), paramConnectionStateChange.getCurrentState()});
        achieveExpectedConnectionState();
    }

    public void onError(String paramString1, String paramString2, Exception paramException) {
        String.format("Connection error: [%s] [%s] [%s]", new Object[]{paramString1, paramString2, paramException});
    }

    public void onEvent(String paramString1, String paramString2, String paramString3) {
        log(paramString3);
    }

    public void onSubscriptionSucceeded(String paramString) {
        String.format("Subscription succeeded for [%s]", new Object[]{paramString});
    }

    static class LogTask extends AsyncTask<Void, Void, Void> {
        String msg;

        public LogTask(String paramString) {
            this.msg = paramString;
        }

        protected Void doInBackground(Void... paramVarArgs) {
            return null;
        }

        protected void onPostExecute(Void paramVoid) {
            System.out.println(this.msg);
            Object localObject = this.msg.replace("\\", "").replace("\"", "");
            ((String) localObject).getBytes();
            String[] arrayOfString = ((String) localObject).split(":");
            if (((String) localObject).contains("PushOrderRequestServer")) {

            }
            Websmith_Pusher.generateNotification(arrayOfString[1]);
            super.onPostExecute(paramVoid);
            return;
        }
    }

}
