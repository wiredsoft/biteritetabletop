package com.smokeysdinein.application;

import android.app.Application;
import android.content.Context;


/**
 * application for base app handling
 * Created by Vineet.Rajpoot on 8/22/2015.
 */
public class SmokeyApplication extends Application {
    public static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }


}
