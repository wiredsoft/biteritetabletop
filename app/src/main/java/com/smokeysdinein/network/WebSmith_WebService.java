package com.smokeysdinein.network;

import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

public class WebSmith_WebService implements WS_Variants {

    SoapObject request;
    Element headerout;
    HttpTransportSE androidHttpTransport;
    String METHOD_NAME, ACTION_NAME, URL, NAMESPACE;
    SoapSerializationEnvelope envelope;

    public WebSmith_WebService(String method, String action) {
        METHOD_NAME = method;
        ACTION_NAME = action;
        URL = HOTEL_URL;
        NAMESPACE = HOTEL_NAMESPACE;
        envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        createSoap();

    }

    @Override
    public void createSoap() {
        request = new SoapObject(NAMESPACE, METHOD_NAME);
    }

    public SoapObject getObject() {
        return request;
    }
    @Override
    public Object call() throws Exception {
        try {
            if (headerout != null) {
                envelope.headerOut = new Element[]{headerout};
            }

            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            androidHttpTransport = new HttpTransportSE(URL, 60000);
            androidHttpTransport.debug = true;

            androidHttpTransport.call(ACTION_NAME, envelope);
            Log.d("request", androidHttpTransport.requestDump);
            Log.d("response", androidHttpTransport.responseDump);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return envelope.getResponse();
    }

    public ArrayHeader createArrayHeader(String headerName) {
        ArrayHeader ah = new ArrayHeader(headerName);
        return ah;
    }

    public class ArrayHeader implements WS_Header_Variants {

        Element element;

        public ArrayHeader(String headerName) {
            element = new Element().createElement(NAMESPACE, headerName);
        }

        @Override
        public Element callHeader(String[][] s, String headerName) {

            Element subelement = new Element().createElement(NAMESPACE,
                    headerName);
            for (int i = 0; i < s.length; i++) {
                Element headerElement = new Element().createElement(NAMESPACE,
                        s[i][0]);
                headerElement.addChild(Node.TEXT, s[i][1]);
                subelement.addChild(Node.ELEMENT, headerElement);
            }

            return subelement;

        }

        @Override
        public Element callHeader(String headerName) {
            Element element = new Element()
                    .createElement(NAMESPACE, headerName);
            return element;

        }

        @Override
        public void addHader(Element parent, Element child) {
            parent.addChild(Node.ELEMENT, child);
        }

        @Override
        public void submit(Element element) {
            headerout.addChild(Node.ELEMENT, element);
        }

        @Override
        public Element getHeaderElement() {
            return element;
        }

    }

    public void callHeader(String headerName, String[][] s) {

        headerout = new Element().createElement(NAMESPACE, headerName);

        headerout.setAttribute(null, "MustUnderstand", "1");
        for (int i = 0; i < s.length; i++) {
            Element headerElement = new Element().createElement(NAMESPACE,
                    s[i][0]);
            headerElement.addChild(Node.TEXT, s[i][1]);
            headerout.addChild(Node.ELEMENT, headerElement);
        }

    }

    public void createHeader(Element element) {

        headerout.addChild(Node.ELEMENT, element);

    }

    public SoapObject createSingleBody(String bodyName) {
        SoapObject object = new SoapObject(NAMESPACE, bodyName);
        return object;
    }

    public SoapObject createBodyArray(String bodyName, String[][] s) {

        SoapObject object = new SoapObject(NAMESPACE, bodyName);
        for (int i = 0; i < s.length; i++) {

            object.addProperty(s[i][0], s[i][1]);
        }
        return object;
    }

    public SoapObject createBodyArray(String bodyName, SoapObject mainObject,
                                      SoapObject subObject) {

        mainObject.addProperty(bodyName, subObject);

        return mainObject;
    }

    public void addBody(String[][] s) {
        for (int i = 0; i < s.length; i++) {
            request.addProperty(s[i][0], s[i][1]);
        }
    }

    public void addBody(String[][] s, String bodyName, SoapObject object) {
        if (s.length > 0) {
            for (int i = 0; i < s.length; i++) {
                request.addProperty(s[i][0], s[i][1]);
            }
        }
        request.addProperty(bodyName, object);
    }

    public void addBody(String bodyName, SoapObject object) {

        request.addProperty(bodyName, object);
    }

}
