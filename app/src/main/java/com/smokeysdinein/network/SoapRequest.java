package com.smokeysdinein.network;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.smokeysdinein.BuildConfig;
import com.smokeysdinein.constatnts.NetworkConstant;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import java.util.HashMap;
import java.util.Map;

/**
 * soap request handling
 * Created by Vineet.Rajpoot on 8/23/2015.
 */
public class SoapRequest {

    private int mApiType;
    private UpdateViewListener mListener;
    private String mSoapAction;
    private final Class mClass;
    private final Gson mGson = new Gson();
    private Activity mActivity;
    private Object mParsingObj;

    public SoapRequest(Activity activity, String method, String action, int apiType, UpdateViewListener listener, String headerName, Class model, HashMap param) {

        mListener = listener;
        mApiType = apiType;
        mSoapAction = action;
        mClass = model;
        mActivity = activity;
        SoapObject request = new SoapObject(NetworkConstant.HOTEL_NAMESPACE, method);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.headerOut = new Element[]{callHeader(headerName, param)};
        makeRequest(request, envelope);
    }

    public SoapRequest(Activity activity, String method, String action, int apiType, UpdateViewListener listener, String headerName, Class model, HashMap param, String key, String value) {

        mListener = listener;
        mApiType = apiType;
        mSoapAction = action;
        mClass = model;
        mActivity = activity;
        SoapObject request = new SoapObject(NetworkConstant.HOTEL_NAMESPACE, method);
        request.addProperty(key, value);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.headerOut = new Element[]{callHeader(headerName, param)};
        makeRequest(request, envelope);
    }


    public Element callHeader(String headerName, HashMap<String, String> param) {

        Element headerout = new Element().createElement(NetworkConstant.HOTEL_NAMESPACE, headerName);

        headerout.setAttribute(null, "MustUnderstand", "1");
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Element headerElement = new Element().createElement(NetworkConstant.HOTEL_NAMESPACE,
                    entry.getKey());
            headerElement.addChild(Node.TEXT, entry.getValue());
            headerout.addChild(Node.ELEMENT, headerElement);
        }
        return headerout;
    }

    private void makeRequest(final SoapObject request, final SoapSerializationEnvelope envelope) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE httpTransport = new HttpTransportSE(
                            NetworkConstant.HOTEL_URL, 60000);
                    httpTransport.debug = true;

                    httpTransport
                            .call(mSoapAction,
                                    envelope);
                    Object result = envelope.getResponse();
                    if (BuildConfig.DEBUG) {
                        Log.d("request", httpTransport.requestDump);
                        Log.d("response", httpTransport.responseDump);
                    }
                    if (result != null) {
                        mParsingObj = mGson.fromJson(result.toString(), mClass);
                    }
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mParsingObj == null) {
                                mListener.onUpdateView(false, mApiType, null);
                            } else {
                                mListener.onUpdateView(true, mApiType, mParsingObj);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
