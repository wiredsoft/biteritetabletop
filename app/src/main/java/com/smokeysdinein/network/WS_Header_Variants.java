package com.smokeysdinein.network;

import org.kxml2.kdom.Element;

public interface WS_Header_Variants {

    Element callHeader(String[][] s, String headerName);

    Element callHeader(String headerName);

    void addHader(Element parent, Element child);

    void submit(Element element);

    Element getHeaderElement();

}
