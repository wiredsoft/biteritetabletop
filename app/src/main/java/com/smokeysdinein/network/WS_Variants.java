package com.smokeysdinein.network;

import com.smokeysdinein.constatnts.NetworkConstant;

import org.ksoap2.serialization.SoapObject;
import org.kxml2.kdom.Element;

public interface WS_Variants {
    // String HOTEL_URL = "http://admin.grubbrr.com/HotelDataService.asmx";
    // String HOTEL_NAMESPACE = "http://admin.grubbrr.com/";

    String HOTEL_URL = NetworkConstant.BASE_URL + "HotelDataService.asmx";
    String HOTEL_NAMESPACE = NetworkConstant.BASE_NAMESPACE;

    void createSoap();

    Object call() throws Exception;

    void callHeader(String headerName, String[][] s);

    void createHeader(Element element);

    SoapObject createSingleBody(String bodyName);

    SoapObject createBodyArray(String bodyName, String[][] s);

    SoapObject createBodyArray(String bodyName, SoapObject mainObject,
                               SoapObject subObject);

    void addBody(String[][] s);

    void addBody(String[][] s, String bodyName, SoapObject object);

    void addBody(String bodyName, SoapObject object);

}
