package com.smokeysdinein.network;

/**
 * take callback to ui
 * Created by Vineet.Rajpoot on 8/23/2015.
 */
public interface UpdateViewListener {
    public void onUpdateView(boolean isSuccess, int apiType, Object response);
}
