package com.smokeysdinein.ui.dialog;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.adapter.RemoveItemsRecycleViewAdapter;
import com.smokeysdinein.ui.fragment.HomeFragment;
import com.smokeysdinein.utils.Constants;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 05/09/15.
 */
public class RemoveItemDialog extends DialogFragment {

    private RecyclerView mItemsRecyclerView;
    private RemoveItemsRecycleViewAdapter mItemsRecycleViewAdapter;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();
    private HomeFragment mHomeFragment;

    public RemoveItemDialog() {
        //Empty constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        getDialog().getWindow().setLayout(width, height);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the XML view for the help dialog fragment
        View view = inflater.inflate(R.layout.dialog_bestwith, container);
        menuItems.addAll(getArguments().<MenuItem>getParcelableArrayList("items"));
        RelativeLayout rt_lyt_bestwith = (RelativeLayout) view.findViewById(R.id.rt_lyt_bestwith);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mItemsRecyclerView = (RecyclerView) view.findViewById(R.id.bestwith_recycleview);
        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setVisibility(View.GONE);
        view.findViewById(R.id.btn_close).setOnClickListener(mCloseBtn);
        mItemsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mItemsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mItemsRecycleViewAdapter = new RemoveItemsRecycleViewAdapter(getActivity(), menuItems, mRemoveClick);
        mItemsRecyclerView.setAdapter(mItemsRecycleViewAdapter);
        mHomeFragment = (HomeFragment) getParentFragment();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float scale = displayMetrics.density;
        int height = displayMetrics.heightPixels;
        int dpAsPixels = (int) (height / 20 * scale + 0.5f);
        rt_lyt_bestwith.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
        return view;
    }

    private View.OnClickListener mCloseBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDialog().dismiss();
        }


    };

    private View.OnClickListener mRemoveClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = (int) v.getTag();
            if (menuItems.get(0).addonsItemIndex != null && menuItems.get(0).addonsItemIndex.size() > 0) {
                for (int i = 0; i < menuItems.get(0).addonsItemIndex.size(); i++) {
                    int position = menuItems.get(0).addonsItemIndex.get(i);
                    if (index == position) {
                        menuItems.get(0).addonsItemIndex.remove(i);
                    }
                }
            }
            mHomeFragment.canceItemOrder(menuItems.get(0), index);

            menuItems.remove(index);
            mItemsRecycleViewAdapter.notifyItemRemoved(index);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mItemsRecycleViewAdapter.notifyDataSetChanged();
                }
            }, 500);

            getActivity().sendBroadcast(new Intent(Constants.NOTIFY_ADAPTERS));
            if (menuItems == null || menuItems.size() <= 0) {
                dismiss();
            }
        }
    };

}