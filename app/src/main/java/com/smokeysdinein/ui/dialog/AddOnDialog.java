package com.smokeysdinein.ui.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.fragment.FilteredItemsFragment;
import com.smokeysdinein.ui.fragment.TodaysSpecialFragment;
import com.smokeysdinein.utils.StringUtils;
import com.smokeysdinein.utils.UiUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * trending dialog
 * Created by Vineet.Rajpoot on 8/24/2015.
 */
public class AddOnDialog extends DialogFragment implements View.OnClickListener {
    private LinearLayout addOnsLayout, extraLayout;
    private TextView txtTotalAmount;
    private MenuItem menuItem;
    int count = 0;
    List<String> addOnList = new ArrayList<>();
    private View view;

    public AddOnDialog() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the XML view for the help dialog fragment
        view = inflater.inflate(R.layout.dialog_add_on, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ImageView imgAddon = (ImageView) view.findViewById(R.id.imgAddon);
        RelativeLayout rt_lyt_bestwith = (RelativeLayout) view.findViewById(R.id.rt_lyt_addons);
        extraLayout = (LinearLayout) view.findViewById(R.id.lyt_extra);
        addOnsLayout = (LinearLayout) view.findViewById(R.id.lyt_addons);
        txtTotalAmount = (TextView) view.findViewById(R.id.txtTotalAmount);
        view.findViewById(R.id.txt_confirm).setOnClickListener(this);
        view.findViewById(R.id.txt_cancel).setOnClickListener(this);
        view.findViewById(R.id.edtSpecialRequest).setFocusable(false);

        view.findViewById(R.id.edtSpecialRequest).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });


        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float scale = displayMetrics.density;
        int height = displayMetrics.heightPixels;
        int dpAsPixels = (int) (height / 10 * scale + 0.5f);
        rt_lyt_bestwith.setPadding(0, dpAsPixels, 0, dpAsPixels / 2);


        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        menuItem = getArguments().getParcelable("menuitem");
        UiUtil.loadImage(getActivity(), imgAddon, menuItem.ItemImage);
        txtTotalAmount.setText("$ " + menuItem.Price);
        addItemInExtra();
        addItemInAddons();
        return view;
    }


    private void addItemInAddons() {

        String extras = menuItem.Extras;
        if (TextUtils.isEmpty(extras)) {
            return;
        }

        Extra = extras.split(",");
        extraLayout.removeAllViews();
        for (int i = 0; i < Extra.length; i++) {


            String extra = Extra[i];
            View view = getActivity().getLayoutInflater().inflate(R.layout.extra_addons, null);
            TextView tv = (TextView) view.findViewById(R.id.txt_extra);
            CheckBox selector = (CheckBox) view.findViewById(R.id.addonselector);
            selector.setTag(i);
            tv.setText(extra);
            extraLayout.addView(view);
            selector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int index = (int) buttonView.getTag();
                    String addon = Extra[index];
                    if (isChecked) {
                        addOnList.add(addon);
                        count++;
                    } else {
                        addOnList.remove(addon);
                        count--;
                    }
                }
            });
        }
    }

    String[] Extra;

    private void addItemInExtra() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                dismiss();
                break;
            case R.id.txt_confirm:

                if (count > 0) {
                    if (menuItem.addonsItemIndex == null) {
                        menuItem.addonsItemIndex = new ArrayList<>();
                    }
                    menuItem.addonsItemIndex.add(menuItem.orderCount);
                }
                if (addOnList.size() > 0) {
                    String extra = "";
                    int i = 0;
                    for (String addon : addOnList) {
                        if (i == 0) {
                            extra = extra + addon;
                        } else {
                            extra = extra + ", " + addon;
                        }
                        i++;
                    }
                    String specialRequest = ((EditText) view.findViewById(R.id.edtSpecialRequest)).getText().toString();
                    if (!StringUtils.isNullOrEmpty(specialRequest)) {
                        if (!StringUtils.isNullOrEmpty(extra)) {
                            extra = extra + ", " + specialRequest;
                        } else {
                            extra = specialRequest;
                        }
                    }


                    if (menuItem.addonsItem == null) {
                        menuItem.addonsItem = new ArrayList<>();
                    }
                    menuItem.addonsItem.add(extra);
                }
                menuItem.orderCount = menuItem.orderCount + 1;
                Fragment fragment = getParentFragment();
                if (fragment instanceof FilteredItemsFragment) {
                    ((FilteredItemsFragment) fragment).onAddToCart(menuItem);
                }
                if (fragment instanceof TodaysSpecialFragment) {
                    ((TodaysSpecialFragment) fragment).onAddToCart(menuItem);
                }
                dismiss();
                break;
        }
    }
}
