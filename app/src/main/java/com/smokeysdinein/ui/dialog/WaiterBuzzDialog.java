package com.smokeysdinein.ui.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.smokeysdinein.R;
import com.smokeysdinein.ui.activity.MainActivity;

/**
 * trending dialog
 * Created by Vineet.Rajpoot on 8/24/2015.
 */
public class WaiterBuzzDialog extends DialogFragment {

    private View view;
    private View.OnClickListener onClickListener;
    private int mLayout;

    public WaiterBuzzDialog() {
        //Empty constructor
    }

    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogFadeAnimation;
        getDialog().setCanceledOnTouchOutside(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the XML view for the help dialog fragment

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        if (mLayout == R.layout.dialog_buzz_waiter) {
            view = inflater.inflate(R.layout.dialog_buzz_waiter, container);
            setBuzzWaiterView();
        } else if (mLayout == R.layout.dialog_get_my_bill) {
            view = inflater.inflate(R.layout.dialog_get_my_bill, container);
            setGetMyBillView();
        } else if (mLayout == R.layout.dialog_get_water) {
            view = inflater.inflate(R.layout.dialog_get_water, container);
            setGetWaterView();
        }

        return view;
    }

    private void setGetWaterView() {
        view.findViewById(R.id.rtlRequestAgain).setOnClickListener(onClickListener);
        view.findViewById(R.id.txtGotIt).setOnClickListener(onClickListener);
    }

    private void setBuzzWaiterView() {
        view.findViewById(R.id.rtlCallWaiter).setOnClickListener(onClickListener);
        view.findViewById(R.id.rtlGetMyBill).setOnClickListener(onClickListener);
        view.findViewById(R.id.rtlGetWater).setOnClickListener(onClickListener);
        view.findViewById(R.id.txtCancel).setOnClickListener(onClickListener);
    }


    private void setGetMyBillView() {
        view.findViewById(R.id.rtlFeedback).setOnClickListener(onClickListener);
        view.findViewById(R.id.txtDone).setOnClickListener(onClickListener);
    }


    public void setListener(MainActivity mainActivity) {
        onClickListener = mainActivity;
    }

    public void setLayout(int layout) {
        mLayout = layout;
    }
}
