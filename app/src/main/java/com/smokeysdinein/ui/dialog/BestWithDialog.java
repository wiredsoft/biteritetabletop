package com.smokeysdinein.ui.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.adapter.FilteredItemsRecycleViewAdapter;
import com.smokeysdinein.ui.fragment.FilteredItemsFragment;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 05/09/15.
 */
public class BestWithDialog extends DialogFragment {


    private RecyclerView mItemsRecyclerView;
    private FilteredItemsRecycleViewAdapter mItemsRecycleViewAdapter;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();

    public BestWithDialog() {
        //Empty constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the XML view for the help dialog fragment
        View view = inflater.inflate(R.layout.dialog_bestwith, container);
        menuItems.addAll(getArguments().<MenuItem>getParcelableArrayList("filter"));
        RelativeLayout rt_lyt_bestwith = (RelativeLayout) view.findViewById(R.id.rt_lyt_bestwith);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mItemsRecyclerView = (RecyclerView) view.findViewById(R.id.bestwith_recycleview);
        view.findViewById(R.id.btn_close).setOnClickListener(mCloseBtn);
        mItemsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        FilteredItemsFragment filteredItemsFragment = (FilteredItemsFragment) getParentFragment();
        mItemsRecycleViewAdapter = new FilteredItemsRecycleViewAdapter(getActivity(), menuItems, filteredItemsFragment, true);
        mItemsRecyclerView.setAdapter(mItemsRecycleViewAdapter);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float scale = displayMetrics.density;
        int height = displayMetrics.heightPixels;
        int dpAsPixels = (int) (height / 20 * scale + 0.5f);
        rt_lyt_bestwith.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
        return view;
    }

    private View.OnClickListener mCloseBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDialog().dismiss();
        }
    };



}