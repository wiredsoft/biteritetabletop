package com.smokeysdinein.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.service.RegistrationIntentService;
import com.smokeysdinein.ui.dialog.WaiterBuzzDialog;
import com.smokeysdinein.ui.fragment.EntertainmentFragment;
import com.smokeysdinein.ui.fragment.FeedBackFragment;
import com.smokeysdinein.ui.fragment.FilteredItemsFragment;
import com.smokeysdinein.ui.fragment.HomeFragment;
import com.smokeysdinein.ui.fragment.OrderReviewFragment;
import com.smokeysdinein.utils.Constants;
import com.smokeysdinein.utils.PreferenceUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LinearLayout btnCheckout;
    private FilteredItemsFragment itemsFragment;
    private LinearLayout mContinueOrdering_Kitchen;
    private LinearLayout mCheckoutLayout;
    private WaiterBuzzDialog dialog;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.txv_buzzer).setOnClickListener(this);
        findViewById(R.id.set_lyt).setOnClickListener(this);
        if (TextUtils.isEmpty(PreferenceUtil.getResImage())) {
            if (!TextUtils.isEmpty(PreferenceUtil.getResName())) {
                ((TextView) findViewById(R.id.txv_res_name)).setText(PreferenceUtil.getResName());
            } else {
                ((TextView) findViewById(R.id.txv_res_name)).setText("No Name");
            }
            findViewById(R.id.txv_res_name).setVisibility(View.VISIBLE);
            findViewById(R.id.lyt_setting).setVisibility(View.GONE);
        } else {
            findViewById(R.id.txv_res_name).setVisibility(View.GONE);
            findViewById(R.id.lyt_setting).setVisibility(View.VISIBLE);
            ImageView res_logo = (ImageView) findViewById(R.id.lyt_setting);
            Picasso.with(this).load(PreferenceUtil.getResImage()).into(res_logo);
        }
        createTabLayout();
        if (checkPlayServices() && !PreferenceUtil.getPushStatus()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            RegistrationIntentService service = new RegistrationIntentService();
            service.start(this, intent);
        }

    }


    private void createTabLayout() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }
        viewPager.setOffscreenPageLimit(4);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void changePage(int pos) {
        viewPager.setCurrentItem(pos);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "HOME");
        adapter.addFragment(new OrderReviewFragment(), "YOUR ORDER");
        //adapter.addFragment(new ServiceFragment(), "SERVICES");

        adapter.addFragment(new FeedBackFragment(), "FEEDBACK");
        adapter.addFragment(new EntertainmentFragment(), "ENTERTAINMENT");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        findViewById(R.id.btn_tranding).setOnClickListener(this);
//        findViewById(R.id.txt_clearfilter).setOnClickListener(this);
//        mContinueOrdering_Kitchen=(LinearLayout)findViewById(R.id.lyt_bottom_sendtokitchen);
//        mCheckoutLayout=(LinearLayout)findViewById(R.id.lyt_bottom);
//        orderRecycleView = (RecyclerView) findViewById(R.id.orders_recycleview);
//        LinearLayoutManager layoutManager
//                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        orderRecycleView.setLayoutManager(layoutManager);
//        btnCheckout = (LinearLayout) findViewById(R.id.btn_checkout);
//        checkoutOrderAdapter = new CheckoutAdapter(orderList);
//        orderRecycleView.setAdapter(checkoutOrderAdapter);
//        addCategoryFragment();
//        addMenuItemsFragment();
//
//        btnCheckout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addReviewFragment();
//            }
//        });
//
//
// HomeFragment homeFragment=new HomeFragment();
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.replace(R.id.main_container, homeFragment);
//        ft.commit();
    }

    private void playSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        TabLayout.Tab tab = tabLayout.getTabAt(2);
        switch (v.getId()) {
            case R.id.txv_buzzer:
                playSound();
                dialog = new WaiterBuzzDialog();
                dialog.setListener(this);
                dialog.setLayout(R.layout.dialog_buzz_waiter);
                dialog.show(getSupportFragmentManager(), "buzz");
                break;
            case R.id.rtlGetMyBill:
                dialog.dismiss();
                dialog.setLayout(R.layout.dialog_get_my_bill);
                dialog.show(getSupportFragmentManager(), "buzz");
                buzzWaiter("GET_MY_BILL");
                break;
            case R.id.set_lyt:
                Intent intent = new Intent(this, ConfigurationActivity.class);
                intent.putExtra(AppConstant.EXTRA_FROM_MAIN, true);
                startActivityForResult(intent, 1001);
                break;
            case R.id.rtlGetWater:
                dialog.dismiss();
                dialog.setLayout(R.layout.dialog_get_water);
                dialog.show(getSupportFragmentManager(), "buzz");
                buzzWaiter("GET_WATER");
                break;
            case R.id.txtCancel:
                dialog.dismiss();
                break;
            case R.id.rtlCallWaiter:
                dialog.dismiss();
                buzzWaiter("GET_ASSISTANCE");
                break;
            case R.id.rtlRequestAgain:
                break;
            case R.id.txtGotIt:
                dialog.dismiss();
                break;
            case R.id.rtlFeedback:
                dialog.dismiss();
                tab.select();
                break;
            case R.id.txtDone:
                dialog.dismiss();
                tab.select();
                break;

        }
    }


//    @Override
//    public void addOrder(MenuItem menuItem) {
//        int count = 1;
//        if (orders.containsKey(menuItem)) {
//            count = orders.get(menuItem);
//            count++;
//
//        }
//        orders.put(menuItem, count);
//    }
//
//    @Override
//    public void canceItemOrder(MenuItem menuItem) {
//        if (orders.containsKey(menuItem)) {
//
//            int count = orders.get(menuItem);
//            count = count - 1;
//
//            if (count <= 0) {
//                orders.remove(menuItem);
//                return;
//            }
//            orders.put(menuItem, count);
//        }
//
//    }


//    public ArrayList<MenuItem> getdata() {
//
//        ArrayList<MenuItem> orderList = new ArrayList<>();
//        for (MenuItem menuItem : orders.keySet()) {
//            int count = orders.get(menuItem);
//            menuItem.orderCount = count;
//            orderList.add(menuItem);
//        }
//        return orderList;
//    }


    public ArrayList<MenuItem> getdata() {

        ArrayList<MenuItem> orderList = new ArrayList<>();
        for (MenuItem menuItem : orders.keySet()) {
            int count = orders.get(menuItem);
            menuItem.orderCount = count;
            orderList.add(menuItem);
        }
        return orderList;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(MainActivity.class.getSimpleName(), "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    sendBroadcast(new Intent(Constants.REFRESHUI));
                }
            };
            Timer t = new Timer();
            t.schedule(tt, 500);
        }
    }

    class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
