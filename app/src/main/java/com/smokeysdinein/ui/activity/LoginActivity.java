package com.smokeysdinein.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.CommonModel;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.utils.NetworkUtils;
import com.smokeysdinein.utils.PreferenceUtil;

import java.util.HashMap;

public class LoginActivity extends BaseActivity {

    private EditText mEdtUserName, mEdtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        if(!TextUtils.isEmpty(PreferenceUtil.getStaffCode())){
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return;
        }else if (!TextUtils.isEmpty(PreferenceUtil.getUserId())) {
            startActivity(new Intent(this, ConfigurationActivity.class));
            finish();
            return;
        }
        mEdtPass = (EditText) findViewById(R.id.edt_user_pass);
        mEdtUserName = (EditText) findViewById(R.id.edt_user_id);
        findViewById(R.id.btn_Login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Login:
                if ((!TextUtils.isEmpty(mEdtUserName.getText().toString()) && !TextUtils.isEmpty(mEdtPass.getText().toString()))) {
                    loginUser();
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.enter_username_password), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void loginUser() {
        if (!NetworkUtils.isNetworkEnabled(this)) {
            Toast.makeText(LoginActivity.this, "No internet", Toast.LENGTH_SHORT).show();
            return;
        }
        onProgressDialogShow(this, "Loading...");
//        String[][] authValues = {{"UserName", mEdtUserName.getText().toString()}, {"Password", mEdtPass.getText().toString()}};
        new SoapRequest(this, NetworkConstant.METHOD_GET_AUTH, NetworkConstant.SOAP_ACTION_GET_AUTH, NetworkConstant.API_LOGIN, this, "SecureAuthentication", CommonModel.class, getParams(NetworkConstant.API_LOGIN));
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, mEdtUserName.getText().toString());
        params.put(ApiConstants.PARAM.PASSWORD, mEdtPass.getText().toString());
        return params;
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        switch (apiType) {
            case NetworkConstant.API_LOGIN: {
                onProgressDialogClose();
                CommonModel resp = (CommonModel) response;
                if (resp != null && resp.Authentication != null) {
                    if (resp.Authentication.Code == 1) {
                        PreferenceUtil.clearPrefrence();
                        Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_SHORT).show();
                        PreferenceUtil.setUserId(mEdtUserName.getText().toString());
                        PreferenceUtil.setPassword(mEdtPass.getText().toString());
                        if (PreferenceUtil.getTableId() == 0) {
                            startActivity(new Intent(this, ConfigurationActivity.class));
                        } else {
                            startActivity(new Intent(this, WelcomeActivity.class));
                        }

                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, resp.Authentication.Message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }


}
