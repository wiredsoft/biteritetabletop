package com.smokeysdinein.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.network.UpdateViewListener;
import com.smokeysdinein.network.WebSmith_WebService;
import com.smokeysdinein.utils.PreferenceUtil;

import org.ksoap2.serialization.SoapObject;

import java.util.HashMap;

/**
 * base activity extend by every activity
 * Created by Vineet.Rajpoot on 8/22/2015.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, UpdateViewListener {

    public HashMap<MenuItem, Integer> orders = new HashMap<>();
    private ProgressDialog mProgress;
    protected Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        PreferenceUtil.getInstance();


    }


    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void buzzWaiter(final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                WebSmith_WebService ws = new WebSmith_WebService(
                        NetworkConstant.METHOD_SEND_NOTIFICATION,
                        NetworkConstant.SOAP_ACTION_SEND_NOTIFICATION);
                /*SoapObject SendNotification = ws
                        .createSingleBody("SendNotification");*/
                SoapObject obj = ws.getObject();

                obj.addProperty("UserCode", PreferenceUtil.getUserId());
                obj.addProperty("TableID", "" + PreferenceUtil.getTableId() + "");
                obj.addProperty("StaffID", PreferenceUtil.getStaffCode());
                obj.addProperty("Message", message);

                String response = null;
                try {
                    Object o = ws.call();
                    response = o.toString();
                    Log.d("test", response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    public HashMap<MenuItem, Integer> getOrders() {
        return orders;
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {

    }


    public void onProgressDialogClose() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }


    public void onProgressDialogShow(Context context, String msg) {
        mProgress = ProgressDialog.show(context, "", msg);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setCancelable(true);
    }

    public void onProgressDialogShow(Context context, String title, String msg) {
        mProgress = ProgressDialog.show(context, title, msg);
    }


    public void onProgressDialogShow(Context context, String title, boolean isCancelable) {
        mProgress = ProgressDialog.show(context, "", title);
        mProgress.setCanceledOnTouchOutside(isCancelable);
        mProgress.setCancelable(isCancelable);
    }

    public void makeApiCall_CallTheWaiter() {

    }

    public void makeApiCall_GetMeWater() {

    }

    public void makeApiCall_GetMyBill() {

    }
}
