package com.smokeysdinein.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.CategoryModelResponse;
import com.smokeysdinein.model.CommonModel;
import com.smokeysdinein.model.GetMenuItemResponse;
import com.smokeysdinein.model.GetMyProfileResponse;
import com.smokeysdinein.model.TableModal;
import com.smokeysdinein.model.TableModalResponse;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.ui.adapter.TableAdapter;
import com.smokeysdinein.utils.NetworkUtils;
import com.smokeysdinein.utils.PreferenceUtil;
import com.smokeysdinein.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class ConfigurationActivity extends BaseActivity {

    private GridView mGridView;
    private TableAdapter mAdapter;
    private ArrayList<TableModal> mTableList;
    private int mTableId;
    private EditText mStaffCode, mStaffPassword;
    private boolean isUserClearData = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        findViewById(R.id.btn_Login).setOnClickListener(this);
        findViewById(R.id.btn_Logout).setOnClickListener(this);
        mGridView = (GridView) findViewById(R.id.gridview);
        mStaffCode = (EditText) findViewById(R.id.edt_staff_code);
        mStaffPassword = (EditText) findViewById(R.id.edt_pass);
        findViewById(R.id.btn_refresh).setOnClickListener(this);
        findViewById(R.id.btnClearOrder).setOnClickListener(this);
        if (!StringUtils.isNullOrEmpty(PreferenceUtil.getStaffCode())) {
            mStaffCode.setText(PreferenceUtil.getStaffCode());
        }


        getTable();
        getRestaurantProfile();
    }

    private void getRestaurantProfile() {
        if (!NetworkUtils.isNetworkEnabled(this)) {
            Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
            return;
        }
        //onProgressDialogShow(this, "Loading...");
//        String[][] authValues = {{"UserName", mEdtUserName.getText().toString()}, {"Password", mEdtPass.getText().toString()}};
        new SoapRequest(this, NetworkConstant.METHOD_GET_RESTAURANT_PROFILE, NetworkConstant.SOAP_ACTION_GET_RESTAURANT_PROFILE, NetworkConstant.API_REST_PROFILE, this, "SecureAuthentication", GetMyProfileResponse.class, getParams(NetworkConstant.API_REST_PROFILE));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Login: {
                if (!isTableValidate()) {
                    Toast.makeText(this, "Select a table.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mStaffCode.getText().toString())) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mStaffPassword.getText().toString())) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }
                loginWithStaffCode();
                break;
            }
            case R.id.btn_Logout: {
                String staffcode = mStaffCode.getText().toString();
                String pass = mStaffPassword.getText().toString();
                String staffcode1 = PreferenceUtil.getStaffCode();
                String pass1 = PreferenceUtil.getStaffPass();
                if (TextUtils.isEmpty(staffcode1)) {
                    PreferenceUtil.clearPrefrence();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                if (TextUtils.isEmpty(staffcode)) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (staffcode.equalsIgnoreCase(staffcode1) && pass.equalsIgnoreCase(pass1)) {
                    PreferenceUtil.setUserId("");
                    PreferenceUtil.setPassword("");
                    PreferenceUtil.saveStaffCode("");
                    PreferenceUtil.saveStaffPass("");
                    PreferenceUtil.saveTableId(0);
                    PreferenceUtil.clearPrefrence();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Please enter valid credentials!", Toast.LENGTH_SHORT).show();
                }
                break;

            }
            case R.id.btn_refresh:
                if (!TextUtils.isEmpty(PreferenceUtil.getStaffCode())) {
                    loadCategories();
                }
                break;
            case R.id.btnClearOrder:
                String staffcode = mStaffCode.getText().toString();
                String pass = mStaffPassword.getText().toString();
                String staffcode1 = PreferenceUtil.getStaffCode();
                String pass1 = PreferenceUtil.getStaffPass();
                if (TextUtils.isEmpty(staffcode1)) {
                    PreferenceUtil.clearPrefrence();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                if (TextUtils.isEmpty(staffcode)) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    Toast.makeText(this, "Enter staff code.", Toast.LENGTH_SHORT).show();
                    return;
                }

                PreferenceUtil.saveUserOrders("");
                PreferenceUtil.saveTempId("");
                PreferenceUtil.saveBookingId("");
                Toast.makeText(this, "Order Successfully cleared.", Toast.LENGTH_SHORT).show();
                isUserClearData = true;
                //sendClearOrderBroadcast();
                break;
            default:
                break;
        }
    }

    private void sendClearOrderBroadcast() {
        Intent intent = new Intent(AppConstant.LOCAL_BROADCAST_CLEAR_ORDER);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void loginWithStaffCode() {
        if (!NetworkUtils.isNetworkEnabled(this)) {
            Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
            return;
        }
        onProgressDialogShow(this, "Loading...");
//        String[][] authValues = {{"UserName", mEdtUserName.getText().toString()}, {"Password", mEdtPass.getText().toString()}};
        new SoapRequest(this, NetworkConstant.METHOD_GET_AUTH_STAFF, NetworkConstant.SOAP_ACTION_AUTH_STAFF, NetworkConstant.API_AUTH_STAFF, this, "StaffAuthentication", CommonModel.class, getStaffParams());
    }

    private HashMap<String, String> getStaffParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, mStaffCode.getText().toString());
        params.put(ApiConstants.PARAM.PASSWORD, mStaffPassword.getText().toString());
        params.put(ApiConstants.PARAM.REG_ID, "");
        return params;
    }

    private boolean isTableValidate() {
        for (TableModal modal : mTableList) {
            if (modal.isSelected) {
                mTableId = modal.TableID;
                return true;
            }
        }
        return false;
    }

    private void getTable() {
        if (!NetworkUtils.isNetworkEnabled(this)) {
            Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
            return;
        }
        onProgressDialogShow(this, "Loading...");
//        String[][] authValues = {{"UserName", mEdtUserName.getText().toString()}, {"Password", mEdtPass.getText().toString()}};
        new SoapRequest(this, NetworkConstant.METHOD_GET_TABLE, NetworkConstant.SOAP_ACTION_GET_TABLE, NetworkConstant.API_GET_TABLE, this, "SecureAuthentication", TableModalResponse.class, getParams(NetworkConstant.API_LOGIN), "DateTime", "12-9-2015");
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        return params;
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        switch (apiType) {
            case NetworkConstant.API_GET_TABLE: {
                onProgressDialogClose();
                TableModalResponse resp = (TableModalResponse) response;
                if (resp != null && resp.TableStatusResponse != null && !resp.TableStatusResponse.isEmpty()) {
                    mTableList = resp.TableStatusResponse;
                    int i = 0;
                    for (TableModal temp : mTableList) {
                        if (temp.TableID == PreferenceUtil.getTableId()) {
                            mTableId = temp.TableID;
                            mTableList.get(i).isSelected = true;
                        }
                        i++;
                    }
                    mAdapter = new TableAdapter(this, mTableList);
                    mGridView.setAdapter(mAdapter);
                }
                break;
            }
            case NetworkConstant.API_REST_PROFILE: {
                GetMyProfileResponse resp = (GetMyProfileResponse) response;

                if (resp != null && resp.RestaurantProfile != null) {
                    //updateUi(resp.RestaurantProfile);
                    if (!TextUtils.isEmpty(resp.RestaurantProfile.rest_image)) {
                        PreferenceUtil.setResImage(resp.RestaurantProfile.rest_image);
                    }
                    if (!TextUtils.isEmpty(resp.RestaurantProfile.Name)) {
                        PreferenceUtil.setResName(resp.RestaurantProfile.Name);
                    }
                } else {
//                    onProgressDialogClose();
//                    startActivity(new Intent(this, MainActivity.class));
//                    finish();
                }
                break;
            }

            case NetworkConstant.API_AUTH_STAFF: {
                onProgressDialogClose();
                CommonModel resp = (CommonModel) response;
                if (resp != null && resp.Authentication != null) {
                    if (resp.Authentication.Code == 1) {
                        PreferenceUtil.saveStaffCode(mStaffCode.getText().toString());
                        PreferenceUtil.saveStaffPass(mStaffPassword.getText().toString());
                        for (TableModal temp : mTableList) {
                            if (temp.isSelected == true) {
                                mTableId = temp.TableID;
                                PreferenceUtil.saveTableId(temp.TableID);
                            }
                        }
                        if (getIntent() != null) {
                            if (getIntent().getBooleanExtra(AppConstant.EXTRA_FROM_MAIN, false)) {
                                finish();
                                return;
                            }
                        }
                        startActivity(new Intent(this, MainActivity.class));
                        finish();
                    } else {
                        Toast.makeText(this, "Invalid Authentication", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Invalid Authentication", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case NetworkConstant.API_CATEGORY:

                CategoryModelResponse categoryModelResponse = (CategoryModelResponse) response;
                if (categoryModelResponse != null) {
                    String categoriesData = gson.toJson(categoryModelResponse, CategoryModelResponse.class);
                    PreferenceUtil.saveFoodCategories(categoriesData);
                    loadMenuItems();
                } else {
                    onProgressDialogClose();
                    Toast.makeText(ConfigurationActivity.this, "Something went wrong please try Again!", Toast.LENGTH_SHORT).show();
                }
                break;

            case NetworkConstant.API_MENUITEM:
                onProgressDialogClose();
                GetMenuItemResponse getMenuItemResponse = (GetMenuItemResponse) response;
                if (getMenuItemResponse != null) {
                    String menuItemJsonData = gson.toJson(getMenuItemResponse, GetMenuItemResponse.class);
                    PreferenceUtil.saveMenuItems(menuItemJsonData);
                } else {
                    Toast.makeText(ConfigurationActivity.this, "Something went wrong please try Again!", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    private void loadCategories() {
        onProgressDialogShow(this, "Loading Menu Item & Categories");
        new SoapRequest(this, NetworkConstant.METHOD_GET_CATEGORY,
                NetworkConstant.SOAP_ACTION_GET_CATEGORY, NetworkConstant.API_CATEGORY, this, "SecureAuthentication", CategoryModelResponse.class, getParams(NetworkConstant.API_CATEGORY));
    }

    private void loadMenuItems() {

        new SoapRequest(this, NetworkConstant.METHOD_GET_MENU_ITEMS,
                NetworkConstant.SOAP_ACTION_GET_MENU_ITEMS, NetworkConstant.API_MENUITEM, this, "SecureAuthentication", GetMenuItemResponse.class, getParams(NetworkConstant.API_MENUITEM));


    }

    @Override
    public void onBackPressed() {
        if (isUserClearData)
            setResult(RESULT_OK);
        super.onBackPressed();
    }
}
