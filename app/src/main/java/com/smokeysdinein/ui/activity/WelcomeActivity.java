package com.smokeysdinein.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.GetCustomerProfileResponse;
import com.smokeysdinein.model.GetMyProfileResponse;
import com.smokeysdinein.model.GetMyProfileResult;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.utils.NetworkUtils;
import com.smokeysdinein.utils.PreferenceUtil;

import java.util.HashMap;

public class WelcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom);
        getRestaurantProfile();
    }

    private void getRestaurantProfile() {
        if (!NetworkUtils.isNetworkEnabled(this)) {
            Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
            return;
        }
        onProgressDialogShow(this, "Loading...");
//        String[][] authValues = {{"UserName", mEdtUserName.getText().toString()}, {"Password", mEdtPass.getText().toString()}};
        new SoapRequest(this, NetworkConstant.METHOD_GET_RESTAURANT_PROFILE, NetworkConstant.SOAP_ACTION_GET_RESTAURANT_PROFILE, NetworkConstant.API_REST_PROFILE, this, "SecureAuthentication", GetMyProfileResponse.class, getParams());
    }


    private HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        return params;
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        switch (apiType) {
            case NetworkConstant.API_REST_PROFILE: {
                GetMyProfileResponse resp = (GetMyProfileResponse) response;

                if (resp != null && resp.RestaurantProfile != null) {
                    updateUi(resp.RestaurantProfile);
                    if (!TextUtils.isEmpty(resp.RestaurantProfile.rest_image)) {
                        PreferenceUtil.setResImage(resp.RestaurantProfile.rest_image);
                    }

                    getUserProfile();
                } else {
                    onProgressDialogClose();
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }
                break;
            }
            case NetworkConstant.API_GET_CUSTOMER_PROFILE: {
                onProgressDialogClose();
                GetCustomerProfileResponse resp = (GetCustomerProfileResponse) response;
                if (resp != null && resp.CustomerProfileResponse != null) {

                }
            }
        }
    }

    private void getUserProfile() {
        new SoapRequest(this, NetworkConstant.METHOD_GET_CUSTOMER_PROFILE, NetworkConstant.SOAP_ACTION_GET_CUSTOMER_PROFILE, NetworkConstant.API_GET_CUSTOMER_PROFILE, this, "B2CData", GetCustomerProfileResponse.class, getProfileParams());
    }


    private HashMap<String, String> getProfileParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        params.put("TableID", "0");
        params.put("TableRequestID", "0");
        params.put("Mobile", "9871200071");
        params.put("Type", "0");
        return params;
    }

    private void updateUi(GetMyProfileResult restaurantProfile) {
        ((TextView) findViewById(R.id.txv_res_name)).setText(restaurantProfile.Name);
        ((TextView) findViewById(R.id.txv_tag_line)).setText(restaurantProfile.TagLine);
        findViewById(R.id.txv_continue).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


}
