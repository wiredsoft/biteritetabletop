package com.smokeysdinein.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.BookingItemResponse;
import com.smokeysdinein.model.Ingredients;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.model.UpdateOrderResponse;
import com.smokeysdinein.network.WebSmith_WebService;
import com.smokeysdinein.ui.adapter.OrderReviewAdapter;
import com.smokeysdinein.utils.PreferenceUtil;
import com.smokeysdinein.utils.StringUtils;

import org.ksoap2.serialization.SoapObject;
import org.kxml2.kdom.Element;

import java.util.ArrayList;
import java.util.List;

public class OrderActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mOrderRecycleView;
    private RecyclerView.Adapter mOrderAdapter;
    private ArrayList<MenuItem> orders;
    private Handler handler = new Handler();
    private float mPrice = 0;
    private TextView txv_subtotal, txv_total;
    private String mSpecialRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_order_review);
        txv_subtotal = (TextView) findViewById(R.id.txv_subtotal);
        txv_total = (TextView) findViewById(R.id.txv_total);
        findViewById(R.id.txtBack).setOnClickListener(this);
        Intent i = getIntent();
        if (i != null && i.getParcelableArrayListExtra("orders") != null) {
            orders = i.getParcelableArrayListExtra("orders");
            mOrderRecycleView = (RecyclerView) findViewById(R.id.offers_recycleview);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mOrderRecycleView.setLayoutManager(layoutManager);
            mOrderAdapter = new OrderReviewAdapter(orders, this, mEditClick);
            mOrderRecycleView.setAdapter(mOrderAdapter);
        }
        findViewById(R.id.lyt_send_kitchen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oreder();
            }
        });
        getBill();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mClearOrderReceiver, new IntentFilter(AppConstant.LOCAL_BROADCAST_CLEAR_ORDER));
    }

    private BroadcastReceiver mClearOrderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    private float getBill() {
        for (MenuItem menu : orders) {
            mPrice = mPrice + (menu.orderCount * menu.Price);
        }
        txv_subtotal.setText("$ " + String.format("%.2f", mPrice));
        txv_total.setText("$ " + String.format("%.2f", mPrice));
        return mPrice;
    }

    private void oreder() {
        mSpecialRequest = ((EditText) findViewById(R.id.txtSpecialRequest)).getText().toString();
        if (StringUtils.isNullOrEmpty(mSpecialRequest)) {
            mSpecialRequest = "";
        }
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String response;
                if (!StringUtils.isNullOrEmpty(PreferenceUtil.getBookingId())) {
                    response = updateOrders();
                } else {
                    sendToKitchen();
                }

            }
        });
        t.start();
        onProgressDialogShow(this, "Loading...", false);
    }


    private String sendToKitchen() {
        WebSmith_WebService ws = null;
        ws = new WebSmith_WebService(
                NetworkConstant.METHOD_BOOK_ORDER,
                NetworkConstant.SOAP_ACTION_BOOK_ORDER);


        String[][] Values = {{"UserName", PreferenceUtil.getUserId()},
                {"Password", PreferenceUtil.getPassword()}, {"TableID", PreferenceUtil.getTableId() + ""},
                {"StaffID", PreferenceUtil.getStaffCode()},
                {"SpecialRequest", mSpecialRequest},
                {"TipAmount", "0"}, {"PhoneOrder", "0"},
                {"OrderType", "0"}, {"ResponseFormat", "1"},
                {"AddressToPrint", "0"}

        };
        String[][] CustomerDetailsvalues = {{"CustomerID", "0"},
                {"CustomerName", "Guest 1"},
                {"Mobile", "9999999999"},
                {"EmailID", "mobile@web.com"},
                {"DOB", "1990-09-20"}, {"Apartment", ""},
                {"AddressLine2", ""}, {"Street", ""},
                {"City", ""}, {"State", ""}, {"Country", ""},
                {"ZipCode", ""}, {"Address", ""},
                {"D_AddressLine1", ""}, {"D_AddressLine2", ""},
                {"D_Apartment", ""}, {"D_City", ""},
                {"D_State", ""}, {"D_ZipCode", ""}

        };


        ws.callHeader("MenuBookingMaster", Values);


        WebSmith_WebService.ArrayHeader ah = ws.createArrayHeader("OrderLinks");


        Element CustomerDetails = ah.callHeader(
                CustomerDetailsvalues, "CustomerDetails");

        ah.submit(CustomerDetails);
        ah.submit(ah.getHeaderElement());


        SoapObject BookMenuItemOrder = ws
                .createSingleBody("BookMenuItemOrder");
        SoapObject objectItemDetails = ws
                .createSingleBody("ItemDetails");
        for (MenuItem menuItem : orders) {
            String addOns = "";
            if (menuItem.addonsItem != null && menuItem.addonsItem.size() > 0) {
                addOns = menuItem.addonsItem.get(0);
            }
            String[][] tansactionValues = {
                    {"CategoryID", "" + menuItem.CategoryID},
                    {"MenuItemID", "" + menuItem.MenuItemID},
                    {"Quantity", "" + menuItem.orderCount},
                    {"Price", "" + menuItem.Price},
                    {"SpecialRemarks", addOns},

            };
            SoapObject MenuBookingTransaction = ws.createBodyArray(
                    "MenuBookingTransaction", tansactionValues);

            SoapObject IngredientDetails = ws
                    .createSingleBody("IngredientDetails");


            for (Ingredients ingredients : menuItem.Ingredients) {
                String[][] ingredientValues = {
                        {"IngredientID",
                                ingredients.IngredientID + ""},
                        {"Price", ingredients.Price + ""}};

                SoapObject MenuIngredients = ws.createBodyArray(
                        "MenuIngredients", ingredientValues);

                IngredientDetails.addProperty("MenuIngredients",
                        MenuIngredients);

            }
            MenuBookingTransaction.addProperty("IngredientDetails",
                    IngredientDetails);
            ws.createBodyArray("MenuBookingTransaction",
                    BookMenuItemOrder, MenuBookingTransaction);

        }
        ws.createBodyArray("ItemDetails", BookMenuItemOrder,
                objectItemDetails);
        ws.addBody("ItemDetails", BookMenuItemOrder);
        String response = null;
        try {
            Object o = ws.call();
            response = o.toString();
            BookingItemResponse bookingItemResponse = gson.fromJson(response, BookingItemResponse.class);

            if (bookingItemResponse.BookingItemResponse != null) {

                String savedItems = PreferenceUtil.getUserOrders();
                if (!TextUtils.isEmpty(savedItems)) {
                    ArrayList<MenuItem> savedMenu = gson.fromJson(savedItems, new TypeToken<List<MenuItem>>() {
                    }.getType());
                    if (savedMenu != null) {
                        orders.addAll(savedMenu);
                    }

                }
                PreferenceUtil.saveUserOrders(new Gson().toJson(orders));

                PreferenceUtil.saveBookingId(bookingItemResponse.BookingItemResponse.BookingID);
                PreferenceUtil.saveTempId(bookingItemResponse.BookingItemResponse.TempID);
                PreferenceUtil.saveInvoiceNo(bookingItemResponse.BookingItemResponse.InvoiceNo);
                PreferenceUtil.saveCustomerId(bookingItemResponse.BookingItemResponse.CustomerID);

                onProgressDialogClose();
                showMessage("Order has been Successfully Placed.");


            } else {
                onProgressDialogClose();
                showMessage("Please Try Again.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            onProgressDialogClose();
            showMessage("Please Try Again.");
        } finally {


        }

        return response;
    }


    private String updateOrders() {
        WebSmith_WebService ws = null;


        ws = new WebSmith_WebService(
                NetworkConstant.METHOD_GET_UPDATE_ORDER,
                NetworkConstant.SOAP_ACTION_GET_UPDATE_ORDER);

        String bookingId = PreferenceUtil.getBookingId();

        String[][]
                Values = {{"UserName", PreferenceUtil.getUserId()},
                {"Password", PreferenceUtil.getPassword()}, {"TableID", PreferenceUtil.getTableId() + ""},
                {"SpecialRequest", mSpecialRequest},
                {"TempID", PreferenceUtil.getTempId()},
                {"BookingID", bookingId},
                {"ResponseFormat", "1"},
                {"AddressToPrint", "1"}};

        String[][] CustomerDetailsvalues = {
                {"CustomerID", "0"},
                {"CustomerName", "Guest 1"},//username
                {"Mobile", "9999999999"},
                {"EmailID", "" + "temp@gmail.com"},
                {"DOB", "1990-09-20"},
                {"AddressLine2", ""},
                {"Apartment", ""},
                {"Street", ""},
                {"City", ""},
                {"State", ""},
                {"Country", ""},
                {"ZipCode", ""},
                {"Address", ""},
                {"D_AddressLine1", ""},
                {"D_AddressLine2", ""},
                {"D_Apartment", ""},
                {"D_City", ""},
                {"D_State", ""},
                {"D_ZipCode", ""}};

        //String[][] SplitOrderLinkValues = {
        //        {"CustomerName", "Hulk"},
        //        {"Mobile", "9818006048"}, {"LinkID", "0"}};


        ws.callHeader("ChangeMenuBooking", Values);
        WebSmith_WebService.ArrayHeader ah = ws.createArrayHeader("OrderLinks");
        Element CustomerDetails = ah.callHeader(
                CustomerDetailsvalues, "CustomerDetails");

        ah.submit(CustomerDetails);
        // ah.submit(splitOrderLink);
        ah.submit(ah.getHeaderElement());


        SoapObject BookMenuItemOrder = ws
                .createSingleBody("BookMenuItemOrder");
        SoapObject objectItemDetails = ws
                .createSingleBody("ItemDetails");
        for (MenuItem menuItem : orders) {
            String addOns = "";
            if (menuItem.addonsItem != null && menuItem.addonsItem.size() > 0) {
                addOns = menuItem.addonsItem.get(0);
            }

            String[][] tansactionValues = {
                    {"CategoryID", "" + menuItem.CategoryID},
                    {"MenuItemID", "" + menuItem.MenuItemID},
                    {"Quantity", "" + menuItem.orderCount},
                    {"Price", "" + menuItem.Price},
                    {"SpecialRemarks", addOns},

            };
            SoapObject MenuBookingTransaction = ws.createBodyArray(
                    "MenuBookingTransaction", tansactionValues);

            SoapObject IngredientDetails = ws
                    .createSingleBody("IngredientDetails");


            for (Ingredients ingredients : menuItem.Ingredients) {
                String[][] ingredientValues = {
                        {"IngredientID",
                                ingredients.IngredientID + ""},
                        {"Price", ingredients.Price + ""}};

                SoapObject MenuIngredients = ws.createBodyArray(
                        "MenuIngredients", ingredientValues);

                IngredientDetails.addProperty("MenuIngredients",
                        MenuIngredients);

            }
            MenuBookingTransaction.addProperty("IngredientDetails",
                    IngredientDetails);
            ws.createBodyArray("MenuBookingTransaction",
                    BookMenuItemOrder, MenuBookingTransaction);

        }
        ws.createBodyArray("ItemDetails", BookMenuItemOrder,
                objectItemDetails);
        ws.addBody("ItemDetails", BookMenuItemOrder);
        String response = null;
        try {
            Object o = ws.call();
            response = o.toString();
            UpdateOrderResponse updateOrderResponse = null;
            if (!TextUtils.isEmpty(response)) {
                updateOrderResponse = gson.fromJson(response, UpdateOrderResponse.class);
            }

            if (updateOrderResponse.UpdateOrderResponse.BookingID != null) {
                String savedItems = PreferenceUtil.getUserOrders();
                if (!TextUtils.isEmpty(savedItems)) {
                    ArrayList<MenuItem> savedMenu = gson.fromJson(savedItems, new TypeToken<List<MenuItem>>() {
                    }.getType());
                    if (savedMenu != null) {
                        orders.addAll(savedMenu);
                    }

                }
                PreferenceUtil.saveUserOrders(new Gson().toJson(orders));
                onProgressDialogClose();
                showMessage("Order has been Successfully Placed.");


            } else {
                onProgressDialogClose();
                showMessage("Please Try Again.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            onProgressDialogClose();
            showMessage("Please Try Again.");
        } finally {

        }

        return response;
    }

    private View.OnClickListener mEditClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OrderActivity.this.finish();
        }
    };

    private void showMessage(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                orders.clear();
                mOrderAdapter.notifyDataSetChanged();
                Toast.makeText(OrderActivity.this, message, Toast.LENGTH_LONG).show();
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBack:
                finish();
                break;
            default:
                super.onClick(v);
        }

    }
}
