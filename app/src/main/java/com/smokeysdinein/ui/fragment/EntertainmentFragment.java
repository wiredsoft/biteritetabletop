package com.smokeysdinein.ui.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smokeysdinein.R;

import tic_tac.Game;

/**
 * A simple {@link Fragment} subclass.
 */
public class EntertainmentFragment extends BaseFragment {


    public EntertainmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_entertainment, container, false);

        view.findViewById(R.id.btnGame).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGame:
                Intent intent = new Intent(getActivity(), Game.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        super.onClick(view);
    }
}
