package com.smokeysdinein.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.smokeysdinein.R;
import com.smokeysdinein.utils.AlertDialogUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedBackFragment extends BaseFragment {

    private RadioGroup mRadioGrp1, mRadioGrp2, mRadioGrp3;

    public FeedBackFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_back, container, false);
        mRadioGrp1 = (RadioGroup) view.findViewById(R.id.rdgp1);
        mRadioGrp2 = (RadioGroup) view.findViewById(R.id.rdgp2);
        mRadioGrp3 = (RadioGroup) view.findViewById(R.id.rdgp3);
        view.findViewById(R.id.txv_submit).setOnClickListener(this);
        return view;
    }

    private int getRadioPos(int id) {
        switch (id) {
            case R.id.radio_1_1:
                return 1;
            case R.id.radio_1_2:
                return 2;
            case R.id.radio_1_3:
                return 3;
            case R.id.radio_1_4:
                return 4;
            case R.id.radio_1_5:
                return 5;
            case R.id.radio_2_1:
                return 1;
            case R.id.radio_2_2:
                return 2;
            case R.id.radio_2_3:
                return 3;
            case R.id.radio_2_4:
                return 4;
            case R.id.radio_2_5:
                return 5;
            case R.id.radio_3_1:
                return 1;
            case R.id.radio_3_2:
                return 2;
            case R.id.radio_3_3:
                return 3;
            case R.id.radio_3_4:
                return 4;
            case R.id.radio_3_5:
                return 5;
            default:
                return 0;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txv_submit: {
                int radioPos1 = getRadioPos(mRadioGrp1.getCheckedRadioButtonId());
                int radioPos2 = getRadioPos(mRadioGrp2.getCheckedRadioButtonId());
                int radioPos3 = getRadioPos(mRadioGrp3.getCheckedRadioButtonId());
                AlertDialogUtils.showAppDialog(getActivity(), getString(R.string.app_name), "Your feedback submit successfully", this);
                break;
            }
        }
    }
}
