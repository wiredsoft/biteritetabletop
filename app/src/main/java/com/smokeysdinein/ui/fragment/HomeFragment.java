package com.smokeysdinein.ui.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.model.FoodCategory;
import com.smokeysdinein.model.GetMenuItemResponse;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.activity.OrderActivity;
import com.smokeysdinein.ui.adapter.CheckoutAdapter;
import com.smokeysdinein.ui.dialog.RemoveItemDialog;
import com.smokeysdinein.ui.listner.CategorySelectionListener;
import com.smokeysdinein.ui.listner.ItemRemoveListener;
import com.smokeysdinein.ui.listner.MenuItemEventsListener;
import com.smokeysdinein.utils.Constants;
import com.smokeysdinein.utils.PreferenceUtil;
import com.smokeysdinein.utils.UiUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by deepaksharma on 26/08/15.
 */
public class HomeFragment extends BaseFragment implements MenuItemEventsListener, CategorySelectionListener, ViewSwitcher.ViewFactory {


    private LinearLayout sendToKitchenButton;
    private TodaysSpecialFragment todaysSpecialFragment;
    private CategoryFragment categoryFragment;
    private HashMap<MenuItem, ArrayList<MenuItem>> ordersMap = new HashMap<>();
    private RecyclerView ordersRecycleView;
    private CheckoutAdapter checkoutAdapter;
    private LinearLayout today_special_btn;
    private ItemRemoveListener itemRemoveListener;
    private TextView txt_havnt_order;
    private ArrayList<MenuItem> menuItems = null;
    private LinearLayout rootBottom;
    private FloatingActionButton floatingBtnVeg;
    private FloatingActionButton floatingBtnNonVeg;
    private FloatingActionButton floatingBtnClear;
    private Handler handler = new Handler();
    private int a = 0;
    private static final int[] SWITCHER_IMAGE_ARRAY = {R.drawable.cravver_logo_white, R.drawable.cravver_logo_orange};

    private View.OnClickListener mOrderRemoveListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ArrayList<MenuItem> orderList = ordersMap.get((MenuItem) v.getTag());
            RemoveItemDialog removeItemDialog = new RemoveItemDialog();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("items", orderList);
            removeItemDialog.setArguments(bundle);
            removeItemDialog.show(getChildFragmentManager(), "show");
        }
    };
    private BroadcastReceiver refreshUI = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Set<MenuItem> orderset = ordersMap.keySet();
            Iterator<MenuItem> itemIterator = orderset.iterator();
            while (itemIterator.hasNext()) {
                itemIterator.next().orderCount = 0;
            }

            if (ordersMap != null)
                ordersMap.clear();

            checkoutAdapter.notifyDataSetChanged();
            refreshUI();

        }
    };
    private ImageView imgExpandCol;
    private TextView txvCravver;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //handler.postDelayed(timerRunnable, 2000);
    }


    private BroadcastReceiver mClearOrderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ordersMap != null)
                ordersMap.clear();


        }
    };


    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            onSwitch();
            handler.postDelayed(this, 2000);
        }
    };

    private void onSwitch() {
        if (a == 0) {
            a++;
        } else {
            a--;
        }
        //mImageSwitcher.setImageResource(SWITCHER_IMAGE_ARRAY[a]);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ordersRecycleView = (RecyclerView) view.findViewById(R.id.orders_recycleview);


        today_special_btn = (LinearLayout) view.findViewById(R.id.today_special_btn);
        sendToKitchenButton = (LinearLayout) view.findViewById(R.id.sent_to_kitchen_btn);
        txt_havnt_order = (TextView) view.findViewById(R.id.txt_havnt_order);
        imgExpandCol = (ImageView) view.findViewById(R.id.img_exp_col);
        txvCravver = (TextView) view.findViewById(R.id.txv_craver);
        txvCravver.setOnClickListener(this);
        imgExpandCol.setOnClickListener(this);
        rootBottom = (LinearLayout) view.findViewById(R.id.recy_view_root);
        today_special_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTodaysSpecialFragment();
            }
        });

        sendToKitchenButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                launch();
            }
        });
        getMenuItemList();
        addCategoryFragment();
        addTodaysSpecialFragment();

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ordersRecycleView.setLayoutManager(layoutManager);

        checkoutAdapter = new CheckoutAdapter(getActivity(), ordersMap, mOrderRemoveListner);
        ordersRecycleView.setAdapter(checkoutAdapter);
        populateOrders();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_exp_col:
                if (rootBottom.getVisibility() == View.GONE) {
                    imgExpandCol.setImageResource(R.drawable.ic_minus);
                    UiUtil.expand(rootBottom);
                } else {
                    imgExpandCol.setImageResource(R.drawable.ic_plus);
                    UiUtil.collapse(rootBottom);
                }
                break;
        }
    }

    public void launch() {


        Set<MenuItem> orderSet = ordersMap.keySet();
        if (orderSet == null || orderSet.size() <= 0) {
            txt_havnt_order.setTextColor(getResources().getColor(R.color.orange));
            return;
        }
        Intent i = new Intent(getActivity(), OrderActivity.class);
        ArrayList<MenuItem> orderedItems = new ArrayList<>();
        orderedItems.addAll(orderSet);
        i.putParcelableArrayListExtra("orders", orderedItems);
        startActivityForResult(i, 1001);

    }

    private void addTodaysSpecialFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        todaysSpecialFragment = (TodaysSpecialFragment) fragmentManager.findFragmentByTag(TodaysSpecialFragment.class.getName());
        if (todaysSpecialFragment == null)
            todaysSpecialFragment = new TodaysSpecialFragment();
        fragmentTransaction.replace(R.id.item_container, todaysSpecialFragment).addToBackStack(TodaysSpecialFragment.class.getName());
        fragmentTransaction.commit();
    }

    private void addCategoryFragment() {
        categoryFragment = new CategoryFragment();
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.category_container, categoryFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void addOrder(MenuItem menuItem) {
        ArrayList<MenuItem> orderList = null;
        if (ordersMap.containsKey(menuItem)) {
            orderList = ordersMap.get(menuItem);
            orderList.add(menuItem);

        }

        if (orderList == null) {
            orderList = new ArrayList<>();
            orderList.add(menuItem);
        }

        ordersMap.put(menuItem, orderList);

        if (checkoutAdapter != null)
            checkoutAdapter.notifyDataSetChanged();
        txt_havnt_order.setVisibility(View.GONE);
        if (imgExpandCol.getVisibility() == View.GONE) {
            imgExpandCol.setVisibility(View.VISIBLE);
            txvCravver.setVisibility(View.GONE);
            imgExpandCol.setImageResource(R.drawable.ic_plus);
        }

    }

    @Override
    public void canceItemOrder(MenuItem menuItem) {

        if (ordersMap.containsKey(menuItem)) {
            ArrayList<MenuItem> orderList = ordersMap.get(menuItem);
            orderList.remove(menuItem);
            if (orderList.size() <= 0) {
                ordersMap.remove(menuItem);
            }
        }
        if (ordersMap.size() <= 0) {
            imgExpandCol.setVisibility(View.GONE);
            txvCravver.setVisibility(View.VISIBLE);
            txt_havnt_order.setVisibility(View.VISIBLE);
            txt_havnt_order.setTextColor(Color.WHITE);
        }

        checkoutAdapter.notifyDataSetChanged();
    }

    @Override
    public void canceItemOrder(MenuItem menuItem, int index) {
        if (ordersMap.containsKey(menuItem)) {
            ArrayList<MenuItem> orderList = ordersMap.get(menuItem);
            orderList.remove(index);
            menuItem.orderCount = orderList.size();
            if (orderList.size() <= 0) {
                ordersMap.remove(menuItem);
            }
        }
        if (ordersMap.size() <= 0) {
            imgExpandCol.setVisibility(View.GONE);
            txvCravver.setVisibility(View.VISIBLE);
            txt_havnt_order.setVisibility(View.VISIBLE);
            txt_havnt_order.setVisibility(View.VISIBLE);
            txt_havnt_order.setTextColor(Color.WHITE);
        }
        checkoutAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCategorySelection(FoodCategory category) {
        ArrayList<MenuItem> filteredList = new ArrayList<>();
        ArrayList<MenuItem> foodItems = null;
        if (menuItems == null)
            foodItems = getMenuItemList();
        else
            foodItems = menuItems;

        for (MenuItem menu : foodItems) {
            if (menu.CategoryID == category.CategoryID) {
                filteredList.add(menu);
            }
        }

        FilteredItemsFragment filteredItemsFragment = null;
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        filteredItemsFragment = (FilteredItemsFragment) fragmentManager.findFragmentByTag(FilteredItemsFragment.class.getName());
        if (filteredItemsFragment == null)
            filteredItemsFragment = new FilteredItemsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("filter", filteredList);
        filteredItemsFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.item_container, filteredItemsFragment).addToBackStack(FilteredItemsFragment.class.getName());
        fragmentTransaction.commit();

    }

    public void setItemListner(ItemRemoveListener itemRemoveListener) {
        this.itemRemoveListener = itemRemoveListener;
    }

    public ArrayList<MenuItem> getMenuItemList() {
        if (menuItems != null) {
            return menuItems;
        }
        String data = PreferenceUtil.getMenuItems();
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
            GetMenuItemResponse responses = gson.fromJson(data, GetMenuItemResponse.class);
            menuItems = responses.MenuItem;
        }
        return menuItems;
    }

    private void populateOrders() {
        clearUserOrders();
        String ordersData = PreferenceUtil.getUserOrders();
        if (TextUtils.isEmpty(ordersData)) {
            return;
        }

        Type type = new TypeToken<Map<MenuItem, ArrayList<MenuItem>>>() {
        }.getType();
        // HashMap<MenuItem,ArrayList<MenuItem>> orderMap= mGson.fromJson(ordersData,type);
        //populateOrdersFRomDB(orderMap);
    }

    private void populateOrdersFRomDB(HashMap<MenuItem, ArrayList<MenuItem>> orderMap) {
        Set<MenuItem> orderSet = orderMap.keySet();
        for (MenuItem menuItem : orderSet)
            for (MenuItem menu : menuItems) {
                if (menu.MenuItemID == menuItem.MenuItemID) {
                    menu.orderCount = menu.orderCount + 1;
                    if (orderMap.containsKey(menu)) {
                        ArrayList<MenuItem> listOrder = orderMap.get(menu);
                        listOrder.add(menu);
                    } else {
                        ArrayList<MenuItem> menuList = new ArrayList<>();
                        menuList.add(menu);
                        orderMap.put(menu, menuList);
                    }
                }
            }

        checkoutAdapter.notifyDataSetChanged();
        getActivity().sendBroadcast(new Intent(Constants.NOTIFY_ADAPTERS));
    }

    private void clearUserOrders() {
        if (ordersMap != null)
            ordersMap.clear();
    }

    private void refreshUI() {
        getActivity().sendBroadcast(new Intent(Constants.NOTIFY_ADAPTERS));
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mClearOrderReceiver, new IntentFilter(AppConstant.LOCAL_BROADCAST_CLEAR_ORDER));
        IntentFilter intentFilter = new IntentFilter(Constants.REFRESHUI);
        getActivity().registerReceiver(refreshUI, intentFilter);
    }


    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(refreshUI);
    }

    @Override
    public View makeView() {
        ImageView iView = new ImageView(getActivity());
        iView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iView.setLayoutParams(new ImageSwitcher.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        iView.setBackgroundColor(0xFF000000);
        return iView;
    }
}
