package com.smokeysdinein.ui.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import com.google.gson.Gson;
import com.smokeysdinein.network.UpdateViewListener;
import com.smokeysdinein.utils.AlertDialogUtils;

/**
 * Basefragment extended by all fragments
 * Created by Vineet.Rajpoot on 8/22/2015.
 */
public class BaseFragment extends Fragment implements View.OnClickListener, UpdateViewListener, AlertDialogUtils.OnButtonClickListener {
    public Gson mGson = new Gson();

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {

    }

    @Override
    public void onButtonClick(int buttonId) {

    }
}
