package com.smokeysdinein.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.adapter.ReviewOrderAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class ReviewOrderFragment extends BaseFragment {

    private RecyclerView mOrderRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mOrderLayoutManager;
    private ArrayList<MenuItem> orders;
    public ReviewOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_order, container, false);
        orders = getArguments().getParcelableArrayList("orders");
        mOrderRecyclerView = (RecyclerView) view.findViewById(R.id.recy_review_order);
        mOrderLayoutManager = new LinearLayoutManager(getActivity());
        mOrderRecyclerView.setLayoutManager(mOrderLayoutManager);
        TextView txtOrders = (TextView) view.findViewById(R.id.txv_order_detail);
        txtOrders.setText("REVIEW YOUR ORDER : " + orders.size() + "ITEMS");

        setUpOrder();
        return view;
    }

    private void setUpOrder() {
        mOrderRecyclerView.setHasFixedSize(true);

        mAdapter = new ReviewOrderAdapter(orders);
        mOrderRecyclerView.setAdapter(mAdapter);
    }

}
