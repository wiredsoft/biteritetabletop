package com.smokeysdinein.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.CategoryModelResponse;
import com.smokeysdinein.model.FoodCategory;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.ui.adapter.CategoryAdapter;
import com.smokeysdinein.ui.listner.CategorySelectionListener;
import com.smokeysdinein.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deepaksharma on 24/08/15.
 */
public class CategoryFragment extends BaseFragment {

    private RecyclerView mCategoryRecyclerView;
    private CategoryAdapter mCategoryAdapter;
    private ArrayList<FoodCategory> categories = new ArrayList<>();
    private CategorySelectionListener categorySelectionListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categorySelectionListener = (CategorySelectionListener) getParentFragment();

    }

    private void loadCategories() {
        String data = PreferenceUtil.getFoodCategories();
        if (!TextUtils.isEmpty(data)) {
            CategoryModelResponse responses = mGson.fromJson(data, CategoryModelResponse.class);
            categories.addAll(responses.FoodCategory);
            mCategoryAdapter.notifyDataSetChanged();

        } else {
            new SoapRequest(getActivity(), NetworkConstant.METHOD_GET_CATEGORY,
                    NetworkConstant.SOAP_ACTION_GET_CATEGORY, NetworkConstant.API_CATEGORY, this, "SecureAuthentication", CategoryModelResponse.class, getParams(NetworkConstant.API_CATEGORY
            ));
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        return params;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_category, container, false);
        mCategoryRecyclerView = (RecyclerView) view.findViewById(R.id.category_recycleview);
        mCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCategoryAdapter = new CategoryAdapter(getActivity(), categories, onCategoryClick);
        mCategoryRecyclerView.setAdapter(mCategoryAdapter);
        loadCategories();
        return view;
    }


    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        CategoryModelResponse categoryModelResponse = (CategoryModelResponse) response;
        categories.addAll(categoryModelResponse.FoodCategory);
        String categories = mGson.toJson(categoryModelResponse, CategoryModelResponse.class);
        PreferenceUtil.saveFoodCategories(categories);
        mCategoryAdapter.notifyDataSetChanged();
    }

    private View.OnClickListener onCategoryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            invalidateAllSelection();
            categories.get(tag).isSelected = true;
            mCategoryAdapter.notifyDataSetChanged();
            categorySelectionListener.onCategorySelection(categories.get(tag));
        }
    };

    private void invalidateAllSelection() {
        for (FoodCategory temp : categories) {
            temp.isSelected = false;
        }
    }
}
