package com.smokeysdinein.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.GetMenuItemResponse;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.ui.adapter.TodaysSpecialAdapter;
import com.smokeysdinein.ui.adapter.Trending_OffersAdapter;
import com.smokeysdinein.ui.dialog.AddOnDialog;
import com.smokeysdinein.ui.listner.FilterItemClickListener;
import com.smokeysdinein.ui.listner.ItemRemoveListener;
import com.smokeysdinein.ui.listner.MenuItemEventsListener;
import com.smokeysdinein.utils.Constants;
import com.smokeysdinein.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deepaksharma on 26/08/15.
 */
public class TodaysSpecialFragment extends BaseFragment implements FilterItemClickListener, ItemRemoveListener {

    private RecyclerView mTodaySpecialRecycleView;
    private TodaysSpecialAdapter mToadysSpecialAdapter;
    private ArrayList<MenuItem> mToadysSpecialList = new ArrayList<>();

    private RecyclerView mTrendingRightNowView;
    private RecyclerView.Adapter mTrendingRightNowAdapter;
    private ArrayList<MenuItem> mTrendingRightNowList = new ArrayList<>();

    private RecyclerView mOffersRecycleView;
    private RecyclerView.Adapter mOffersAdapter;
    private ArrayList<MenuItem> mOffersList = new ArrayList<>();
    private ProgressBar mMenuProgress;

    private MenuItemEventsListener events;
    private Gson gson = new Gson();
    private ArrayList<MenuItem> mMenus;
    private LinearLayout mTrendingLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        events = (MenuItemEventsListener) getParentFragment();
        ((HomeFragment) getParentFragment()).setItemListner(this);
    }


    private void loadMenuItems() {
        ArrayList<MenuItem> menus = ((HomeFragment) getParentFragment()).getMenuItemList();
        if (menus != null) {
            populateList(menus);

        } else {
            mMenuProgress.setVisibility(View.VISIBLE);
            new SoapRequest(getActivity(), NetworkConstant.METHOD_GET_MENU_ITEMS,
                    NetworkConstant.SOAP_ACTION_GET_MENU_ITEMS, NetworkConstant.API_MENUITEM, this, "SecureAuthentication", GetMenuItemResponse.class, getParams(NetworkConstant.API_MENUITEM));

        }
    }





    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        return params;
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        mMenuProgress.setVisibility(View.GONE);
        GetMenuItemResponse getMenuItemResponse = (GetMenuItemResponse) response;
        String menuItemJsonData = gson.toJson(getMenuItemResponse, GetMenuItemResponse.class);
        PreferenceUtil.saveMenuItems(menuItemJsonData);
        mMenus = getMenuItemResponse.MenuItem;
        populateList(mMenus);


    }

    private void populateList(ArrayList<MenuItem> menuItems) {
        for (MenuItem menuItem : menuItems) {
            if (menuItem.IsTodaysSpecial) {
                mToadysSpecialList.add(menuItem);
            } else if (menuItem.IsTrendingItem) {
                mTrendingRightNowList.add(menuItem);
            } else {
                mOffersList.add(menuItem);
            }
        }
        mToadysSpecialAdapter.notifyDataSetChanged();
        mOffersAdapter.notifyDataSetChanged();
        if (mTrendingRightNowList == null || mTrendingRightNowList.size() <= 0) {
            mTrendingLayout.setVisibility(View.GONE);
        }
        mTrendingRightNowAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_todayspecial, container, false);
        mTrendingLayout = (LinearLayout) view.findViewById(R.id.lyttrending);
        mMenuProgress = (ProgressBar) view.findViewById(R.id.prog_menu);
        initTodaySpecialView(view);
        initTrendingRightNow(view);
        initOffersView(view);

        loadMenuItems();


        return view;

    }

    private void initTodaySpecialView(View view) {
        mTodaySpecialRecycleView = (RecyclerView) view.findViewById(R.id.todayspecial_recycleview);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mTodaySpecialRecycleView.setLayoutManager(layoutManager);
        mToadysSpecialAdapter = new TodaysSpecialAdapter(getActivity(), mToadysSpecialList, this);
        mTodaySpecialRecycleView.setAdapter(mToadysSpecialAdapter);
    }

    private void initOffersView(View view) {
        mOffersRecycleView = (RecyclerView) view.findViewById(R.id.offers_recycleview);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mOffersRecycleView.setLayoutManager(layoutManager);
        mOffersAdapter = new Trending_OffersAdapter(getActivity(), mOffersList, this);
        mOffersRecycleView.setAdapter(mOffersAdapter);
    }

    private void initTrendingRightNow(View view) {
        mTrendingRightNowView = (RecyclerView) view.findViewById(R.id.trending_recycleview);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mTrendingRightNowView.setLayoutManager(layoutManager);
        mTrendingRightNowAdapter = new Trending_OffersAdapter(getActivity(), mTrendingRightNowList, this);
        mTrendingRightNowView.setAdapter(mTrendingRightNowAdapter);
    }




    @Override
    public void onAddToCart(MenuItem menuItem) {
        events.addOrder(menuItem);
        mTrendingRightNowAdapter.notifyDataSetChanged();
        mToadysSpecialAdapter.notifyDataSetChanged();
        mOffersAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBestWith(MenuItem menuItem) {

    }

    @Override
    public void addItemOrders(MenuItem menuItem) {
        events.addOrder(menuItem);
    }

    @Override
    public void cancelItemOrder(MenuItem menuItem) {
        events.canceItemOrder(menuItem);
    }

    @Override
    public void addOns(MenuItem menuItem) {
        AddOnDialog addOnDialog = new AddOnDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("menuitem", menuItem);
        addOnDialog.setArguments(bundle);
        addOnDialog.show(getChildFragmentManager(), "show");
    }

    @Override
    public void cancelItemFromDialog(MenuItem menuItem, int index) {
        events.canceItemOrder(menuItem);
    }

    @Override
    public void onRemove(MenuItem menuItem) {
        mToadysSpecialAdapter.notifyDataSetChanged();
        mOffersAdapter.notifyDataSetChanged();
        mTrendingRightNowAdapter.notifyDataSetChanged();
    }

    private BroadcastReceiver notifyAdapter = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            mToadysSpecialAdapter.notifyDataSetChanged();
            mTrendingRightNowAdapter.notifyDataSetChanged();
            mOffersAdapter.notifyDataSetChanged();
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Constants.NOTIFY_ADAPTERS);
        getActivity().registerReceiver(notifyAdapter, intentFilter);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(notifyAdapter);
    }

}