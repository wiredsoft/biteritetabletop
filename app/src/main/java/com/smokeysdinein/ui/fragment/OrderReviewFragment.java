package com.smokeysdinein.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.network.WebSmith_WebService;
import com.smokeysdinein.ui.activity.MainActivity;
import com.smokeysdinein.ui.adapter.OrderReviewAdapter;
import com.smokeysdinein.utils.PreferenceUtil;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderReviewFragment extends BaseFragment {

    private RecyclerView mOrderRecycleView;
    private RecyclerView.Adapter mOrderAdapter;
    private ArrayList<MenuItem> mOrders = new ArrayList<>();

    private TextView txv_subtotal, txv_total;

    public OrderReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_your_order, container, false);


        txv_subtotal = (TextView) view.findViewById(R.id.txv_subtotal);
        txv_total = (TextView) view.findViewById(R.id.txv_total);
        view.findViewById(R.id.lyt_get_my_bills).setOnClickListener(this);
        view.findViewById(R.id.lyt_add_more_item).setOnClickListener(this);
        mOrderRecycleView = (RecyclerView) view.findViewById(R.id.offers_recycleview);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mOrderRecycleView.setLayoutManager(layoutManager);
        mOrderAdapter = new OrderReviewAdapter(mOrders, getActivity(), null);
        mOrderRecycleView.setAdapter(mOrderAdapter);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String jsonData = PreferenceUtil.getUserOrders();
        if (!TextUtils.isEmpty(jsonData)) {
            ArrayList<MenuItem> list = mGson.fromJson(jsonData, new TypeToken<List<MenuItem>>() {
            }.getType());
            if (list != null) {
                mOrders.clear();
                mOrders.addAll(list);
               // mOrderAdapter.notifyDataSetChanged();
            }

        }else{
            mOrders.clear();
        }
        mOrderAdapter.notifyDataSetChanged();
        getBill();
    }

    private void getBill() {
        if (mOrders == null && mOrders.size() <= 0) {
            return;
        }
        float mPrice = 0;
        for (MenuItem menu : mOrders) {
            mPrice = mPrice + (menu.orderCount * menu.Price);
        }
        txv_subtotal.setText("$ " + String.format("%.2f", mPrice));
        txv_total.setText("$ " + String.format("%.2f", mPrice));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_get_my_bills: {
                getMybill();
                break;
            }
            case R.id.lyt_add_more_item: {
                ((MainActivity) getActivity()).changePage(0);
                break;
            }
        }
    }


    public void getMybill() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                WebSmith_WebService ws = new WebSmith_WebService(
                        NetworkConstant.METHOD_GET_MY_BILL,
                        NetworkConstant.SOAP_ACTION_GET_MY_BILL);
                /*SoapObject SendNotification = ws
                        .createSingleBody("SendNotification");*/
                SoapObject obj = ws.getObject();

                obj.addProperty("UserID", PreferenceUtil.getUserId());
                obj.addProperty("InvoiceNo", PreferenceUtil.getInvoiceNo());

                String response = null;
                try {
                    Object o = ws.call();
                    response = o.toString();
                    Log.d("test", response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

}
