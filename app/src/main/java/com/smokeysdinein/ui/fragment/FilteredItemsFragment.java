package com.smokeysdinein.ui.fragment;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.smokeysdinein.R;
import com.smokeysdinein.model.GetMenuItemResponse;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.model.NameComparator;
import com.smokeysdinein.model.PriceComparator;
import com.smokeysdinein.model.SuggestiveItems;
import com.smokeysdinein.ui.adapter.FilteredItemsRecycleViewAdapter;
import com.smokeysdinein.ui.adapter.GridViewAdapter;
import com.smokeysdinein.ui.adapter.SearchAdapter;
import com.smokeysdinein.ui.dialog.AddOnDialog;
import com.smokeysdinein.ui.dialog.BestWithDialog;
import com.smokeysdinein.ui.listner.FilterItemClickListener;
import com.smokeysdinein.ui.listner.ItemRemoveListener;
import com.smokeysdinein.ui.listner.MenuItemEventsListener;
import com.smokeysdinein.utils.Constants;
import com.smokeysdinein.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p/>
 * create an instance of this fragment.
 */
public class FilteredItemsFragment extends BaseFragment implements FilterItemClickListener, ItemRemoveListener {

    private RecyclerView mItemsRecyclerView;
    private GridView mFilterdItemGridView;
    private FilteredItemsRecycleViewAdapter mItemsRecycleViewAdapter;
    private GridViewAdapter mItemsGridViewAdapter;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();
    private ImageView mChangeItemsView;
    private ArrayList<MenuItem> items;
    private TextView vegFilter, nonVegFilter, allFilter, clearFilter;
    private Spinner sortSpinner;
    private MenuItemEventsListener events;
    private ImageView btn_changeview;
    private RelativeLayout mFilterKeyLayout;
    private SearchView searchView;
    private AutoCompleteTextView autoSearch;
    private SearchAdapter adapter;
    private FloatingActionMenu floatingActionButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        events = (MenuItemEventsListener) getParentFragment();
        ((HomeFragment) getParentFragment()).setItemListner(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_items, container, false);
        items = getArguments().<MenuItem>getParcelableArrayList("filter");
        menuItems.addAll(items);
        floatingActionButton = (FloatingActionMenu) view.findViewById(R.id.menu2);

        view.findViewById(R.id.floatingBtnVeg).setOnClickListener(this);
        view.findViewById(R.id.floatingBtnNonVeg).setOnClickListener(this);
        view.findViewById(R.id.floatingBtnClear).setOnClickListener(this);

        mFilterdItemGridView = (GridView) view.findViewById(R.id.item_gridView);
        mItemsRecyclerView = (RecyclerView) view.findViewById(R.id.items_recycleview);
        mChangeItemsView = (ImageView) view.findViewById(R.id.btn_changeview);
        vegFilter = (TextView) view.findViewById(R.id.txt_vegfilter);
        nonVegFilter = (TextView) view.findViewById(R.id.txt_nonvegfilter);
        allFilter = (TextView) view.findViewById(R.id.all);
        clearFilter = (TextView) view.findViewById(R.id.clear_filter);
        sortSpinner = (Spinner) view.findViewById(R.id.spinner);
        mFilterKeyLayout = (RelativeLayout) view.findViewById(R.id.filter_key_layout);
        btn_changeview = (ImageView) view.findViewById(R.id.btn_changeview);
        searchView = (SearchView) view.findViewById(R.id.searchview);
        autoSearch = (AutoCompleteTextView) view.findViewById(R.id.auto_search);
        adapter = new SearchAdapter(getActivity(), menuItems);
        autoSearch.setAdapter(adapter);
        autoSearch.setThreshold(2);
        autoSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onTextChangedListner(((MenuItem) parent.getItemAtPosition(position)).ItemName);
            }
        });
        autoSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {
                    menuItems.clear();
                    menuItems.addAll(items);
                    mItemsRecycleViewAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onTextChangedListner(newText);
                return true;
            }
        });

        clearFilter.setOnClickListener(this);
        allFilter.setOnClickListener(this);
        nonVegFilter.setOnClickListener(this);
        vegFilter.setOnClickListener(this);
        view.findViewById(R.id.lnrChangeView).setOnClickListener(this);
        mChangeItemsView.setOnClickListener(this);
        initAdapters();
        addSpinner();
        return view;
    }

    private void onTextChangedListner(String s) {
        ArrayList<MenuItem> items = getArguments().<MenuItem>getParcelableArrayList("filter");
        menuItems.clear();
        if (s != null && s.length() <= 0) {
            menuItems.addAll(items);
        } else {
            for (int i = 0; i < items.size(); i++) {
                MenuItem menuItem = items.get(i);
                String name = menuItem.ItemName.toUpperCase();
                s = s.toUpperCase();
                if (name.contains(s)) {
                    menuItems.add(menuItem);
                }
            }
        }
        onSortSpinnerSelection(sortSpinner.getSelectedItemPosition());
        mItemsRecycleViewAdapter.notifyDataSetChanged();
    }

    private void initAdapters() {
        mItemsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mItemsRecycleViewAdapter = new FilteredItemsRecycleViewAdapter(getActivity(), menuItems, this, false);
        mItemsGridViewAdapter = new GridViewAdapter(getActivity(), menuItems, this);
        mItemsRecyclerView.setAdapter(mItemsRecycleViewAdapter);
        mFilterdItemGridView.setAdapter(mItemsGridViewAdapter);
        mItemsRecyclerView.addOnScrollListener(mScrollListner);
    }

    private void addSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_txt, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        sortSpinner.setAdapter(adapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onSortSpinnerSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void onSortSpinnerSelection(int posotion) {
        switch (posotion) {
            case 2:
                Collections.sort(menuItems, new PriceComparator());
                mItemsRecycleViewAdapter.notifyDataSetChanged();
                mItemsGridViewAdapter.notifyDataSetChanged();
//                mFilterdItemGridView.notifyAll();

                break;
            case 1:
                Collections.sort(menuItems, new NameComparator());
                mItemsRecycleViewAdapter.notifyDataSetChanged();
                mItemsGridViewAdapter.notifyDataSetChanged();
//                mFilterdItemGridView.notifyAll();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        //  resetFilterBg();
        switch (v.getId()) {
            case R.id.floatingBtnVeg:
                floatingActionButton.performClick();
                //vegFilter.setTextColor(Color.WHITE);
                addVegFilter();
                break;
            case R.id.floatingBtnNonVeg:
                floatingActionButton.performClick();
                //nonVegFilter.setTextColor(Color.WHITE);
                addNonVegFilter();
                break;
            case R.id.all:
                floatingActionButton.performClick();
                allFilter.setTextColor(Color.WHITE);
                allFilter.setBackgroundResource(R.color.orange);
                clearFilters();
                break;
            case R.id.floatingBtnClear:
                floatingActionButton.performClick();
                //  clearFilter.setTextColor(Color.WHITE);
                //  clearFilter.setBackgroundResource(R.color.orange);
                clearFilters();
                break;
            case R.id.lnrChangeView:
            case R.id.btn_changeview:
                changeWindowView();
                break;

        }
    }

//    private void resetFilterBg() {
//        vegFilter.setBackgroundResource(Color.TRANSPARENT);
//        nonVegFilter.setBackgroundResource(Color.TRANSPARENT);
//        clearFilter.setBackgroundResource(Color.TRANSPARENT);
//        allFilter.setBackgroundResource(Color.TRANSPARENT);
//
//        vegFilter.setTextColor(Color.BLACK);
//        nonVegFilter.setTextColor(Color.BLACK);
//        clearFilter.setTextColor(Color.BLACK);
//        allFilter.setTextColor(Color.BLACK);
//
//    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
    }

    public View.OnClickListener addToPlate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MenuItem menuItem = menuItems.get((int) v.getTag());
            events.addOrder(menuItem);
        }
    };

    private void addVegFilter() {
        // vegFilter.setBackgroundResource(R.color.orange);
        ArrayList<MenuItem> items = getArguments().<MenuItem>getParcelableArrayList("filter");
        menuItems.clear();
        for (int i = 0; i < items.size(); i++) {
            MenuItem menuItem = items.get(i);
            if (!menuItem.IsNonVeg) {
                menuItems.add(menuItem);
            }
        }

        onSortSpinnerSelection(sortSpinner.getSelectedItemPosition());
        mItemsRecycleViewAdapter.notifyDataSetChanged();
    }

    private void addNonVegFilter() {
        // nonVegFilter.setBackgroundResource(R.color.orange);
        ArrayList<MenuItem> items = getArguments().<MenuItem>getParcelableArrayList("filter");
        menuItems.clear();
        for (int i = 0; i < items.size(); i++) {
            MenuItem menuItem = items.get(i);
            if (menuItem.IsNonVeg) {
                menuItems.add(menuItem);
            }
        }
        onSortSpinnerSelection(sortSpinner.getSelectedItemPosition());
        mItemsRecycleViewAdapter.notifyDataSetChanged();
    }

    private void changeWindowView() {
        if (mFilterdItemGridView.getVisibility() == View.GONE) {
            mFilterdItemGridView.setVisibility(View.VISIBLE);
            mItemsRecyclerView.setVisibility(View.GONE);
        } else {
            mFilterdItemGridView.setVisibility(View.GONE);
            mItemsRecyclerView.setVisibility(View.VISIBLE);
            btn_changeview.setImageResource(R.drawable.list_icon);
        }
    }

    private void clearFilters() {
        menuItems.clear();
        menuItems.addAll(items);
        mItemsRecycleViewAdapter.notifyDataSetChanged();
        mItemsGridViewAdapter.notifyDataSetChanged();
    }


    @Override
    public void onAddToCart(MenuItem menuItem) {
        events.addOrder(menuItem);
        mItemsRecycleViewAdapter.notifyDataSetChanged();
        mItemsGridViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBestWith(MenuItem menuItem) {
        ArrayList<SuggestiveItems> suggestiveItemses = (ArrayList<SuggestiveItems>) menuItem.SuggestiveItems;
        ArrayList<MenuItem> bestWithList = null;
        if (suggestiveItemses != null && suggestiveItemses.size() > 0) {
            bestWithList = new ArrayList<MenuItem>();
            ArrayList<MenuItem> menus = getFoodItems();
            for (MenuItem menu : menus) {
                for (SuggestiveItems suggestiveItem : suggestiveItemses) {
                    if (suggestiveItem.ItemID == menu.MenuItemID) {
                        bestWithList.add(menu);
                    }
                }
                if (bestWithList.size() == suggestiveItemses.size()) {
                    break;
                }
            }

            if (bestWithList.size() > 0) {
                BestWithDialog bestWithDialog = new BestWithDialog();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("filter", bestWithList);
                bestWithDialog.setArguments(bundle);
                bestWithDialog.show(getChildFragmentManager(), "bestWithDialog");
            }

        }

    }

    public ArrayList<MenuItem> getFoodItems() {
        String data = PreferenceUtil.getMenuItems();
        GetMenuItemResponse getMenuItemResponse = new Gson().fromJson(data, GetMenuItemResponse.class);
        return getMenuItemResponse.MenuItem;
    }

    @Override
    public void addItemOrders(MenuItem menuItem) {
        events.addOrder(menuItem);
    }

    @Override
    public void cancelItemOrder(MenuItem menuItem) {
        events.canceItemOrder(menuItem);
    }

    @Override
    public void addOns(MenuItem menuItem) {
        AddOnDialog addOnDialog = new AddOnDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("menuitem", menuItem);
        addOnDialog.setArguments(bundle);
        addOnDialog.show(getChildFragmentManager(), "show");
    }

    @Override
    public void cancelItemFromDialog(MenuItem menuItem, int index) {

        events.canceItemOrder(menuItem);
    }

    @Override
    public void onRemove(MenuItem menuItem) {
        mItemsGridViewAdapter.notifyDataSetChanged();
        mItemsRecycleViewAdapter.notifyDataSetChanged();
    }

    private RecyclerView.OnScrollListener mScrollListner = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                mFilterKeyLayout.setVisibility(View.GONE);
            }

            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFilterKeyLayout.setVisibility(View.GONE);
                    }
                }, 500);

            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

        }
    };

    private Handler handler = new Handler();


    private BroadcastReceiver notifyAdapter = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            mItemsGridViewAdapter.notifyDataSetChanged();
            mItemsRecycleViewAdapter.notifyDataSetChanged();
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Constants.NOTIFY_ADAPTERS);
        getActivity().registerReceiver(notifyAdapter, intentFilter);
    }


    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(notifyAdapter);
    }


}
