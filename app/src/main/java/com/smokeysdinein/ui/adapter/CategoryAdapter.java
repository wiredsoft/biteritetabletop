package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.FoodCategory;

import java.util.ArrayList;

/**
 * category recyclerview adapter
 * Created by Vineet.Rajpoot on 8/23/2015.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private ArrayList<FoodCategory> categories;
    private View.OnClickListener onCategoryClick;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mCatName;
        // public ImageView mCatImg;
        public LinearLayout mRootCategory;

        public ViewHolder(View v) {
            super(v);
            mCatName = (TextView) v.findViewById(R.id.txv_cat_name);
            //  mCatImg = (ImageView) v.findViewById(R.id.img_cat);
            mRootCategory = (LinearLayout) v.findViewById(R.id.root_category);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoryAdapter(Context context, ArrayList<FoodCategory> categories, View.OnClickListener onCategoryClick) {
        this.context = context;
        this.categories = categories;
        this.onCategoryClick = onCategoryClick;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_category, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FoodCategory category = categories.get(position);
        holder.mCatName.setText(category.Category);
        holder.mRootCategory.setTag(position);
        holder.mRootCategory.setOnClickListener(onCategoryClick);

        if (category.isSelected == true) {
            holder.mRootCategory.setBackgroundColor(context.getResources().getColor(R.color.orange));
            holder.mCatName.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.mRootCategory.setBackgroundColor(context.getResources().getColor(R.color.tranding_bg_2));
            holder.mCatName.setTextColor(context.getResources().getColor(R.color.orange));
        }
        // Picasso.with(context).load(ApiConstants.CAT_IMAGE_URI + category.Image).into(holder.mCatImg);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return categories.size();
    }
}
