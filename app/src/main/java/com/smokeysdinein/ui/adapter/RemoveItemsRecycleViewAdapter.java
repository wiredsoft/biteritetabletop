package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.picasso.ImageTrans_RoundedCorner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 23/08/15.
 */
public class RemoveItemsRecycleViewAdapter extends RecyclerView.Adapter<RemoveItemsRecycleViewAdapter.ViewHolder> {

    private ArrayList<MenuItem> menuItems;
    private Context context;
    private View.OnClickListener listener;

    public RemoveItemsRecycleViewAdapter(Context context, ArrayList<MenuItem> menuItems, View.OnClickListener listener) {
        this.menuItems = menuItems;
        this.context = context;
        this.listener = listener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeader;
        public TextView txtDescription;
        public ImageView imageItem;
        public TextView txtPrice;
        public TextView txtRemove;
        public TextView txtExtras;

        public ViewHolder(View itemView) {
            super(itemView);
            txtHeader = (TextView) itemView.findViewById(R.id.txt_header);
            txtDescription = (TextView) itemView.findViewById(R.id.txt_description);
            imageItem = (ImageView) itemView.findViewById(R.id.item_image);
            txtPrice = (TextView) itemView.findViewById(R.id.txt_price);
            txtRemove = (TextView) itemView.findViewById(R.id.txt_remove_item);
            txtExtras = (TextView) itemView.findViewById(R.id.txt_extras);
            txtRemove.setOnClickListener(listener);


        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RemoveItemsRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.remove_item_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RemoveItemsRecycleViewAdapter.ViewHolder holder, int position) {
        MenuItem menuItem = menuItems.get(position);
        holder.txtHeader.setText(menuItem.ItemName);
        holder.txtDescription.setText(menuItem.Description);
        holder.txtPrice.setText("PRICE : $" + String.format("%.2f", menuItem.Price));
        holder.txtRemove.setTag(position);
        if (menuItem.addonsItem != null && menuItem.addonsItem.size() > 0) {

            for (int i = 0; i < menuItem.addonsItemIndex.size(); i++) {
                int index = menuItem.addonsItemIndex.get(i);
                if (index == position) {
                    String extra = "";
                    extra = extra + menuItem.addonsItem.get(i);
                    holder.txtExtras.setText("Extra : " + extra);
                }
            }

        }
        Picasso.with(context).load(ApiConstants.ITEM_IMAGE_URI + menuItem.ItemImage).transform(new ImageTrans_RoundedCorner()).resize(240, 200).centerCrop().into(holder.imageItem);
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuItems.size();
    }

}
