package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.listner.FilterItemClickListener;
import com.smokeysdinein.utils.UiUtil;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 26/08/15.
 */
public class Trending_OffersAdapter extends RecyclerView.Adapter<Trending_OffersAdapter.ViewHolder> {

    private ArrayList<MenuItem> menuItems;
    private Context context;
    private FilterItemClickListener listener;

    public Trending_OffersAdapter(Context context, ArrayList<MenuItem> menuItems, FilterItemClickListener listener) {
        this.menuItems = menuItems;
        this.context = context;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Trending_OffersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_offer_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(Trending_OffersAdapter.ViewHolder holder, int position) {
        MenuItem menuItem = menuItems.get(position);
        holder.addToCart.setTag(position);
        holder.rtlAddOns.setTag(position);
        holder.minus.setTag(position);
        holder.plus.setTag(position);
        holder.txtOrderCount.setTag(position);
        holder.txtAddOns.setTag(position);
        if (TextUtils.isEmpty(menuItem.Extras)) {
            holder.txtAddOns.setEnabled(false);
            holder.txtAddOns.setVisibility(View.INVISIBLE);
        } else {
            holder.txtAddOns.setEnabled(true);
            holder.txtAddOns.setVisibility(View.VISIBLE);
        }
        String name = menuItem.ItemName;


        if (menuItem.orderCount > 0) {
            holder.rtlAddOns.setVisibility(View.VISIBLE);
            holder.addToCart.setVisibility(View.GONE);
        } else {
            holder.rtlAddOns.setVisibility(View.GONE);
            holder.addToCart.setVisibility(View.VISIBLE);
        }
        holder.txtMenuName.setText(name);
        holder.txtMenuPrice.setText("$ " + menuItem.Price);
        holder.txtOrderCount.setText(menuItem.orderCount + "");
        UiUtil.loadImage(context, holder.imageItem, menuItem.ItemImage);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageItem;

        public RelativeLayout addToCart;
        public LinearLayout rtlAddOns;
        public ImageView minus;
        public ImageView plus;
        public TextView txtOrderCount;
        public TextView txtAddOns;
        public TextView txtMenuName;
        public TextView txtMenuPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            imageItem = (ImageView) itemView.findViewById(R.id.item_image);
            addToCart = (RelativeLayout) itemView.findViewById(R.id.rtlAddToCart);
            rtlAddOns = (LinearLayout) itemView.findViewById(R.id.rtlAddOns);
            minus = (ImageView) itemView.findViewById(R.id.imgMinus);
            plus = (ImageView) itemView.findViewById(R.id.imgPlus);

            txtOrderCount = (TextView) itemView.findViewById(R.id.txtQuantity);
            txtAddOns = (TextView) itemView.findViewById(R.id.txtAddOns);

            txtMenuName = (TextView) itemView.findViewById(R.id.txt_menu_name);
            txtMenuPrice = (TextView) itemView.findViewById(R.id.txt_menu_price);

            addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = (Integer) v.getTag();
                    MenuItem menuItem = menuItems.get(index);
                    listener.addOns(menuItem);
                    //listener.addItemOrders(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //rtlAddOns.setVisibility(View.VISIBLE);
                    //addToCart.setVisibility(View.GONE);
                    //Trending_OffersAdapter.this.notifyItemChanged(index);
                }
            });


            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    if (menuItem.orderCount > 0) {
                        listener.cancelItemOrder(menuItem);
                        menuItem.orderCount = menuItem.orderCount - 1;
                        ViewGroup row = (ViewGroup) v.getParent();
                        TextView tv = (TextView) row.getChildAt(1);
                        tv.setText(menuItem.orderCount + "");
                        if (menuItem.orderCount <= 0) {
                            rtlAddOns.setVisibility(View.GONE);
                            addToCart.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    //listener.addItemOrders(menuItem);
                    listener.addOns(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //ViewGroup row = (ViewGroup) v.getParent();
                    //TextView tv = (TextView) row.getChildAt(1);
                    //tv.setText(menuItem.orderCount + "");
                }
            });

            txtAddOns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    listener.addOns(menuItem);
                }
            });
        }


    }
}
