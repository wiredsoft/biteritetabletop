package com.smokeysdinein.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;

import java.util.ArrayList;

/**
 * Created by Vineet.Rajpoot on 8/23/2015.
 */
public class ReviewOrderAdapter extends RecyclerView.Adapter<ReviewOrderAdapter.ViewHolder> {

    private ArrayList<MenuItem> orders;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeader;
        public TextView txtDescription;
        public ImageView imageItem;

        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) itemView.findViewById(R.id.txv_dish_name);
            txtDescription = (TextView) itemView.findViewById(R.id.txv_instruction_label);
            imageItem = (ImageView) itemView.findViewById(R.id.img_item);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReviewOrderAdapter(ArrayList<MenuItem> orders) {
        this.orders = orders;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReviewOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review_order, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuItem menuItem = orders.get(position);
        holder.txtHeader.setText(menuItem.ItemName);
        holder.txtDescription.setText(menuItem.Description);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return orders.size();
    }
}
