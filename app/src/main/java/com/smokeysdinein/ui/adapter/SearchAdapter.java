package com.smokeysdinein.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;

import java.util.ArrayList;

/**
 * Created by vineet.rajpoot on 24-02-2015.
 * Adapter used for city autocomplete with filter
 */
public class SearchAdapter extends ArrayAdapter<MenuItem> {

    private LayoutInflater mInflator;

    private ArrayList<MenuItem> items;

    /**
     * @param activity
     */
    public SearchAdapter(Activity activity, ArrayList<MenuItem> item) {
        super(activity, R.layout.search_item, item);
        mInflator = activity.getLayoutInflater();
        items = item;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.search_item, null);
            holder = new ViewHolder();
            holder.txvPlan = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MenuItem model = getItem(position);
        holder.txvPlan.setText(model.ItemName);
        return convertView;
    }


    /**
     * @author vineet.rajpoot
     */
    private class ViewHolder {
        private TextView txvPlan;
    }
}
