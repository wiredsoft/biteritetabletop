package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.picasso.ImageTrans_RoundedCorner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Vineet.Rajpoot on 8/31/2015.
 */
public class OrderReviewAdapter extends RecyclerView.Adapter<OrderReviewAdapter.ViewHolder> {
    private ArrayList<MenuItem> orders;
    private Context mContext;
    private View.OnClickListener mEditClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderReviewAdapter(ArrayList<MenuItem> orders, Context conteext, View.OnClickListener editClick) {
        this.orders = orders;
        this.mContext = conteext;
        mEditClick = editClick;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_review_item, parent, false);
        parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuItem menuItem = orders.get(position);
        holder.txvTotalPrice.setText("$ " + String.format("%.2f", menuItem.orderCount * menuItem.Price));
        holder.txtPrice.setText("$ " + String.format("%.2f", menuItem.Price));
        holder.txvOrderCount.setText(menuItem.orderCount + "");
        holder.txvTitle.setText(menuItem.ItemName);
        holder.txvSubTitle.setText(menuItem.Description);
        if (mEditClick == null) {
            holder.txt_edit.setVisibility(View.GONE);
        }
        holder.txt_edit.setOnClickListener(mEditClick);
        Picasso.with(mContext).load(ApiConstants.ITEM_IMAGE_URI + menuItem.ItemImage).transform(new ImageTrans_RoundedCorner()).fit().into(holder.imageItem);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return orders.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txvTotalPrice;
        public TextView txvTitle;
        public TextView txvOrderCount;
        public TextView txtPrice;
        public TextView txvSubTitle;
        public ImageView imageItem;
        public TextView txt_edit;

        public ViewHolder(View v) {
            super(v);
            imageItem = (ImageView) itemView.findViewById(R.id.img_order);
            txvTotalPrice = (TextView) itemView.findViewById(R.id.txt_total_price);
            txvTitle = (TextView) itemView.findViewById(R.id.txv_title);
            txvOrderCount = (TextView) itemView.findViewById(R.id.txv_order_count);
            txtPrice = (TextView) itemView.findViewById(R.id.item_price);
            txvSubTitle = (TextView) itemView.findViewById(R.id.txv_sub_title);
            txt_edit = (TextView) itemView.findViewById(R.id.txt_edit);


        }
    }
}
