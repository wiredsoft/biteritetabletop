package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.ui.listner.FilterItemClickListener;
import com.smokeysdinein.utils.UiUtil;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 05/09/15.
 */
public class GridViewAdapter extends BaseAdapter {

    private ArrayList<MenuItem> menuItems;
    private Context context;
    private LayoutInflater mLayoutInflater;
    private FilterItemClickListener listener;

    public GridViewAdapter(Context context, ArrayList<MenuItem> menuItems, FilterItemClickListener listener) {
        this.context = context;
        this.menuItems = menuItems;
        mLayoutInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater
                    .inflate(R.layout.filtered_item_grid_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageItem = (ImageView) convertView.findViewById(R.id.item_image);
            viewHolder.rtlAddOns = (LinearLayout) convertView.findViewById(R.id.rtlAddOns);
            viewHolder.minus = (ImageView) convertView.findViewById(R.id.imgMinus);
            viewHolder.plus = (ImageView) convertView.findViewById(R.id.imgPlus);
            viewHolder.txtOrderCount = (TextView) convertView.findViewById(R.id.txtQuantity);
            viewHolder.txtAddOns = (TextView) convertView.findViewById(R.id.txtAddOns);
            viewHolder.txtMenuName = (TextView) convertView.findViewById(R.id.txt_menu_name);
            viewHolder.addToCart=(RelativeLayout)convertView.findViewById(R.id.rtlAddToCart);
            viewHolder.txtMenuPrice = (TextView) convertView.findViewById(R.id.txt_menu_price);
            viewHolder.addToCart.setOnClickListener(viewHolder);
            viewHolder.minus.setOnClickListener(viewHolder);
            viewHolder.plus.setOnClickListener(viewHolder);
            viewHolder.txtAddOns.setOnClickListener(viewHolder);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        MenuItem menuItem = menuItems.get(position);
        viewHolder.rtlAddOns.setTag(position);
        viewHolder.minus.setTag(position);
        viewHolder.plus.setTag(position);
        viewHolder.addToCart.setTag(position);

        viewHolder.txtAddOns.setTag(position);
        if (TextUtils.isEmpty(menuItem.Extras)) {
            viewHolder.txtAddOns.setEnabled(false);
            viewHolder.txtAddOns.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.txtAddOns.setEnabled(true);
            viewHolder.txtAddOns.setVisibility(View.VISIBLE);
        }
        String name = menuItem.ItemName;
        if (name.length() > 16) {
            name = name.substring(0, 16);
        }

        if(menuItem.orderCount>0){
            viewHolder.rtlAddOns.setVisibility(View.VISIBLE);
            viewHolder.addToCart.setVisibility(View.GONE);
        }else{
            viewHolder.rtlAddOns.setVisibility(View.GONE);
            viewHolder.addToCart.setVisibility(View.VISIBLE);
        }
        viewHolder.txtOrderCount.setText(menuItem.orderCount + "");
        viewHolder.txtMenuName.setText(name);
        viewHolder.txtMenuPrice.setText("$ "+menuItem.Price);
        UiUtil.loadImage(context, viewHolder.imageItem, menuItem.ItemImage);
        return convertView;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder implements View.OnClickListener {
        public ImageView imageItem;

        public RelativeLayout addToCart;
        public LinearLayout rtlAddOns;
        public ImageView minus;
        public ImageView plus;
        public TextView txtOrderCount;
        public TextView txtAddOns;
        public TextView txtMenuName;
        public TextView txtMenuPrice;

        @Override
        public void onClick(View v) {
            MenuItem menuItem = menuItems.get((Integer) v.getTag());
            switch (v.getId()) {
                case R.id.imgPlus:
                    listener.addOns(menuItem);
                    //listener.addItemOrders(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //ViewGroup row = (ViewGroup) v.getParent();
                    //TextView tv = (TextView) row.getChildAt(1);
                    //tv.setText(menuItem.orderCount + "");
                    break;
                case R.id.txtAddOns:
                    listener.addOns(menuItem);
                    break;
                case R.id.imgMinus:
                    if (menuItem.orderCount > 0) {
                        menuItem.orderCount = menuItem.orderCount - 1;
                        ViewGroup parent = (ViewGroup) v.getParent();
                        TextView quantity = (TextView) parent.getChildAt(1);
                        quantity.setText(menuItem.orderCount + "");
                        listener.cancelItemOrder(menuItem);
                        if(menuItem.orderCount<=0){
                            rtlAddOns.setVisibility(View.GONE);
                            addToCart.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                case R.id.rtlAddToCart:
                    int index=(Integer) v.getTag();

                    listener.addOns(menuItem);
                    //listener.addItemOrders(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //rtlAddOns.setVisibility(View.VISIBLE);
                    //addToCart.setVisibility(View.GONE);
                    //GridViewAdapter.this.notifyDataSetChanged();
                    break;
            }
        }
    }


}
