package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.picasso.ImageTrans_RoundedCorner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by deepaksharma on 25/08/15.
 */
public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.ViewHolder> {

    private HashMap<MenuItem, ArrayList<MenuItem>> orders = new HashMap<>();
    private Context context;
    private View.OnClickListener removeClick;
    // Provide a suitable constructor (depends on the kind of dataset)
    public CheckoutAdapter(Context context, HashMap<MenuItem, ArrayList<MenuItem>> orders, View.OnClickListener removeClick) {
        this.orders = orders;
        this.context = context;
        this.removeClick = removeClick;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CheckoutAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkout_order_column, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Set<MenuItem> set = orders.keySet();
        ArrayList<MenuItem> list = new ArrayList<>();
        list.addAll(set);
        MenuItem menuItem = list.get(position);
        holder.item.setTag(menuItem);
        holder.item.setOnClickListener(removeClick);
        holder.badgeView.setText(menuItem.orderCount + "");
        Picasso.with(context).load(ApiConstants.ITEM_IMAGE_URI + menuItem.ItemImage).transform(new ImageTrans_RoundedCorner()).fit().into(holder.mItemImg);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return orders.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mItemImg;
        public TextView badgeView;
        public RelativeLayout item;

        public ViewHolder(View v) {
            super(v);
            mItemImg = (ImageView) v.findViewById(R.id.checkout_image);
            badgeView = (TextView) v.findViewById(R.id.txt_remove_item);
            item=(RelativeLayout)v.findViewById(R.id.root_checkout);
        }
    }
}