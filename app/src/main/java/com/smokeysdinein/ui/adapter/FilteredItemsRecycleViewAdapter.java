package com.smokeysdinein.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.model.MenuItem;
import com.smokeysdinein.picasso.ImageTrans_RoundedCorner;
import com.smokeysdinein.ui.listner.FilterItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by deepaksharma on 23/08/15.
 */
public class FilteredItemsRecycleViewAdapter extends RecyclerView.Adapter<FilteredItemsRecycleViewAdapter.ViewHolder> {

    private ArrayList<MenuItem> menuItems;
    private Context context;
    private FilterItemClickListener listener;
    private boolean isFromBestWtihDialog;

    public FilteredItemsRecycleViewAdapter(Context context, ArrayList<MenuItem> menuItems, FilterItemClickListener listener, boolean isFromBestWtihDialog) {
        this.menuItems = menuItems;
        this.context = context;
        this.isFromBestWtihDialog = isFromBestWtihDialog;
        this.listener = listener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeader;
        public TextView txtDescription;
        public ImageView imageItem;
        public TextView txtPrice;
        public TextView txtBestWith;
        public LinearLayout rootLayout;
        public RelativeLayout rtlAddToCart;
        public LinearLayout rtlAddOns;
        public ImageView minus;
        public ImageView plus;
        public TextView txtOrderCount;
        public TextView txtAddOns;

        public ViewHolder(View itemView) {
            super(itemView);
            txtHeader = (TextView) itemView.findViewById(R.id.txt_header);
            txtDescription = (TextView) itemView.findViewById(R.id.txt_description);
            imageItem = (ImageView) itemView.findViewById(R.id.item_image);
            txtPrice = (TextView) itemView.findViewById(R.id.txt_price);
            txtBestWith = (TextView) itemView.findViewById(R.id.txt_best_with);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.root_filter_item_layout);
            minus = (ImageView) itemView.findViewById(R.id.imgMinus);
            plus = (ImageView) itemView.findViewById(R.id.imgPlus);
            txtOrderCount = (TextView) itemView.findViewById(R.id.txtQuantity);
            rtlAddToCart = (RelativeLayout) itemView.findViewById(R.id.rtlAddToCart);
            rtlAddOns = (LinearLayout) itemView.findViewById(R.id.rtlAddOns);
            txtAddOns = (TextView) itemView.findViewById(R.id.txtAddOns);
            txtBestWith.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    listener.onBestWith(menuItem);
                }
            });

            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    if (menuItem.orderCount > 0) {
                        listener.cancelItemOrder(menuItem);
                        menuItem.orderCount = menuItem.orderCount - 1;
                        ViewGroup row = (ViewGroup) v.getParent();
                        TextView tv = (TextView) row.getChildAt(1);
                        tv.setText(menuItem.orderCount + "");
                        if (menuItem.orderCount < 1) {
                            rtlAddToCart.setVisibility(View.VISIBLE);
                            rtlAddOns.setVisibility(View.GONE);
                        }
                    }
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    //listener.addItemOrders(menuItem);
                    listener.addOns(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //ViewGroup row = (ViewGroup) v.getParent();
                    //TextView tv = (TextView) row.getChildAt(1);
                    //tv.setText(menuItem.orderCount + "");

                }
            });

            txtAddOns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MenuItem menuItem = menuItems.get((Integer) v.getTag());
                    listener.addOns(menuItem);
                }
            });

            rtlAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = (Integer) v.getTag();
                    MenuItem menuItem = menuItems.get(index);
                    listener.addOns(menuItem);
                    //listener.addItemOrders(menuItem);
                    //menuItem.orderCount = menuItem.orderCount + 1;
                    //rtlAddOns.setVisibility(View.VISIBLE);
                    //rtlAddToCart.setVisibility(View.GONE);
                    //FilteredItemsRecycleViewAdapter.this.notifyItemChanged(index);
                }
            });

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public FilteredItemsRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(FilteredItemsRecycleViewAdapter.ViewHolder holder, int position) {
        MenuItem menuItem = menuItems.get(position);
        holder.txtHeader.setText(menuItem.ItemName);
        holder.txtDescription.setText(menuItem.Description);
        holder.txtPrice.setText("PRICE : $" + menuItem.Price);
        holder.minus.setTag(position);
        holder.plus.setTag(position);
        holder.txtOrderCount.setText(menuItem.orderCount + "");
        holder.txtAddOns.setTag(position);
        holder.rtlAddToCart.setTag(position);
        if (isFromBestWtihDialog || menuItem.SuggestiveItems == null || menuItem.SuggestiveItems.size() <= 0) {
            holder.txtBestWith.setVisibility(View.GONE);
        } else {
            holder.txtBestWith.setTag(position);
        }

        if (TextUtils.isEmpty(menuItem.Extras)) {
            holder.txtAddOns.setEnabled(false);
            holder.txtAddOns.setVisibility(View.INVISIBLE);

        } else {
            holder.txtAddOns.setEnabled(true);
            holder.txtAddOns.setVisibility(View.VISIBLE);
        }

        if (menuItem.orderCount > 0) {
            holder.rtlAddToCart.setVisibility(View.GONE);
            holder.rtlAddOns.setVisibility(View.VISIBLE);
        } else {
            holder.rtlAddToCart.setVisibility(View.VISIBLE);
            holder.rtlAddOns.setVisibility(View.GONE);
        }
        Picasso.with(context).load(ApiConstants.ITEM_IMAGE_URI + menuItem.ItemImage).transform(new ImageTrans_RoundedCorner()).resize(240, 200).centerCrop().into(holder.imageItem);
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return menuItems.size();
    }

}
