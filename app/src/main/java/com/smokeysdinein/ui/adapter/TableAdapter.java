package com.smokeysdinein.ui.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.TableModal;

import java.util.ArrayList;

/**
 * Created by Vineet.Rajpoot on 9/12/2015.
 */
public class TableAdapter extends BaseAdapter {
    private ArrayList<TableModal> tables;
    private Activity mActivity;

    // Constructor
    public TableAdapter(Activity c, ArrayList<TableModal> list) {
        mActivity = c;
        tables = list;
    }

    public int getCount() {
        return tables.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.ite_table, parent, false);
            // initialize the view holder
            viewHolder = new ViewHolder();
            viewHolder.tableName = (TextView) convertView.findViewById(R.id.table_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final TableModal modal = tables.get(position);

        viewHolder.tableName.setText(modal.Caption);
        //true means empty false means filled
        if (modal.Status == false) {
            if (modal.isSelected == true) {
                viewHolder.tableName.setBackgroundColor(mActivity.getResources().getColor(R.color.yellow));
            } else {
                viewHolder.tableName.setBackgroundColor(mActivity.getResources().getColor(R.color.orange_dark));
            }
        } else {
            if (modal.isSelected == true) {
                viewHolder.tableName.setBackgroundColor(mActivity.getResources().getColor(R.color.yellow));
            } else {
                viewHolder.tableName.setBackgroundColor(mActivity.getResources().getColor(R.color.white));
            }
            viewHolder.tableName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invalidateSelection();
                    modal.isSelected = true;
                    notifyDataSetChanged();
                }
            });
        }

        return convertView;
    }

    private void invalidateSelection() {
        for (TableModal modal : tables) {
            modal.isSelected = false;
        }
    }

    private static class ViewHolder {
        TextView tableName;
    }
}
