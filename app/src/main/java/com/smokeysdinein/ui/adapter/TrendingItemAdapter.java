package com.smokeysdinein.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smokeysdinein.R;
import com.smokeysdinein.model.TrandingItem;

import java.util.ArrayList;

/**
 * tranding adapter
 * Created by Vineet.Rajpoot on 8/24/2015.
 */

public class TrendingItemAdapter extends RecyclerView.Adapter<TrendingItemAdapter.ViewHolder> {
    private ArrayList<TrandingItem> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTrendingItemName;
        public TextView mTrendicCat;
        public LinearLayout mLayTop;

        public ViewHolder(View v) {
            super(v);
            mTrendingItemName = (TextView) v.findViewById(R.id.item_name);
            mTrendicCat = (TextView) v.findViewById(R.id.item_cat);
            mLayTop = (LinearLayout) v.findViewById(R.id.lay_bg);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TrendingItemAdapter(ArrayList<TrandingItem> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TrendingItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tranding_dialog, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TrandingItem item = mDataset.get(position);
        holder.mTrendingItemName.setText(item.ItemName);
        holder.mTrendicCat.setText(item.ShortDescription);
        if (position % 2 == 0) {
            holder.mLayTop.setBackgroundResource(R.color.tranding_bg_1);
        } else {
            holder.mLayTop.setBackgroundResource(R.color.tranding_bg_2);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
