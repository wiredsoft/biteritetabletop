package com.smokeysdinein.ui.listner;

import com.smokeysdinein.model.FoodCategory;

/**
 * Created by deepaksharma on 27/08/15.
 */
public interface CategorySelectionListener {
    void onCategorySelection(FoodCategory category);
}
