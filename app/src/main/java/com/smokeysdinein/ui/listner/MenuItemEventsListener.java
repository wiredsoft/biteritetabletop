package com.smokeysdinein.ui.listner;

import com.smokeysdinein.model.MenuItem;

/**
 * Created by deepaksharma on 26/08/15.
 */
public interface MenuItemEventsListener {
    void addOrder(MenuItem menuItem);
    void canceItemOrder(MenuItem menuItem);

    void canceItemOrder(MenuItem menuItem, int index);
}
