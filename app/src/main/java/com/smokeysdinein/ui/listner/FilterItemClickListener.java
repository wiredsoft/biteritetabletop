package com.smokeysdinein.ui.listner;

import com.smokeysdinein.model.MenuItem;

/**
 * Created by deepaksharma on 06/09/15.
 */
public interface FilterItemClickListener {
    void onAddToCart(MenuItem menuItem);

    void onBestWith(MenuItem menuItem);

    void addItemOrders(MenuItem menuItem);

    void cancelItemOrder(MenuItem menuItem);

    void addOns(MenuItem menuItem);

    void cancelItemFromDialog(MenuItem menuItem, int index);
}
