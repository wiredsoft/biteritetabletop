package com.smokeysdinein.ui.listner;

import com.smokeysdinein.model.MenuItem;

/**
 * Created by Vineet.Rajpoot on 9/8/2015.
 */
public interface ItemRemoveListener {
    void onRemove(MenuItem menuItem);
}
