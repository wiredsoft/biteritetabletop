package com.smokeysdinein.constatnts;

import android.os.Build;

/**
 * all network constants define here
 * Created by Vineet.Rajpoot on 8/22/2015.
 */
public interface NetworkConstant {

    String BASE_URL = "http://appdevoff.cravrr.com/";
    String BASE_NAMESPACE = "http://appdevoff.cravrr.com/";

    String HOTEL_URL = BASE_URL + "HotelDataService.asmx";
    String HOTEL_NAMESPACE = BASE_NAMESPACE;


    String METHOD_GET_CATEGORYWISE_MENU_ITEMS = "GetCategoryWiseMenuItems";
    String SOAP_ACTION_GET_CATEGORYWISE_MENU_ITEMS = "http://app.cravrr.com/GetCategoryWiseMenuItems";

    String METHOD_GET_MENU_ITEMS = "GetMenuItems";
    String SOAP_ACTION_GET_MENU_ITEMS = BASE_URL + "GetMenuItems";

    String METHOD_GET_MENU_ITEMS_CAT_INGREDIENT = "GetCategoryWiseMenuItemsWithIngredients";
    String SOAP_ACTION_GET_MENU_ITEMS_CAT_INGREDIENT = "http://app.cravrr.com/GetCategoryWiseMenuItemsWithIngredients";

    String METHOD_GET_CATEGORY = "GetFoodCategories";
    String SOAP_ACTION_GET_CATEGORY = BASE_URL + "GetFoodCategories";

    String METHOD_GET_MENU_ITEMS_INGREDIENT = "GetItemWiseIngredients";
    String SOAP_ACTION_GET_MENU_ITEMS_INGREDIENT = BASE_URL + "GetItemWiseIngredients";

    String METHOD_BOOK_ORDER = "BookMenuItemOrder";
    String SOAP_ACTION_BOOK_ORDER = BASE_URL + "BookMenuItemOrder";

    String METHOD_APPROX_TIME = "GetApproxQueueTimeList";
    String SOAP_ACTION_APPROX_TIME = BASE_URL + "GetApproxQueueTimeList";

    String METHOD_GET_AUTH = "CheckAuthentication";
    String SOAP_ACTION_GET_AUTH = BASE_URL + "CheckAuthentication";

    String METHOD_GET_AUTH_STAFF = "AuthenticateStaff";
    String SOAP_ACTION_AUTH_STAFF = BASE_URL + "AuthenticateStaff";

    String METHOD_WAIT_TABLE = "GetWaitingTables";
    String SOAP_ACTION_WAIT_TABLE = BASE_URL + "GetWaitingTables";

    String METHOD_GET_TABLE = "GetTablesWithStatus";
    String SOAP_ACTION_GET_TABLE = BASE_URL + "GetTablesWithStatus";

    String METHOD_DIVICE_REG = "DeviceRegistration";
    String SOAP_ACTION_DIVICE_REG = BASE_URL + "DeviceRegistration";

    String METHOD_BOOK_B2C = "B2cBookTable";
    String SOAP_ACTION_BOOK_B2C = BASE_URL + "B2cBookTable";

    String METHOD_GET_MENU_ORDER = "GetMenuItemOrder";
    String SOAP_ACTION_MENU_ORDER = BASE_URL + "GetMenuItemOrder";

    String METHOD_GET_MENU_SUGGESTIVE = "GetCategoryWiseMenuItemsWithIngredients";
    String SOAP_ACTION_MENU_SUGGESTIVE = BASE_URL + "GetCategoryWiseMenuItemsWithIngredients";

    String METHOD_GET_TRANDING = "GetAllTrendingItems";
    String SOAP_ACTION_TRANDING = BASE_URL + "GetAllTrendingItems";

    String METHOD_GET_NOTIFICATION = "SendPushNotification";
    String SOAP_ACTION_NOTIFICATION = BASE_URL + "SendPushNotification";

    String METHOD_CANCEL_TABLE = "UpdateTableStatus";
    String SOAP_ACTION_CANCEL_TABLE = "http://app.cravrr.com/UpdateTableStatus";

    String FIRST_DATA_NAMESPACE = "http://app.cravrr.com/";
    String FIRST_DATA_URL = "http://app.cravrr.com/service1.asmx";

    String METHOD_FIRSTDATA = "FirstData";
    String SOAP_ACTION_FIRST_DATA = "http://app.cravrr.com/FirstData";

    String METHOD_GET_STAFF = "AuthenticateServer";
    String SOAP_ACTION_GET_STAFF = "http://app.cravrr.com/AuthenticateServer";

    String METHOD_GET_SEATED_TABLE = "GetSeatedWithStatus";
    String SOAP_ACTION_GET_SEATED_TABLE = "http://app.cravrr.com/GetSeatedWithStatus";

    String METHOD_GET_UPDATE_ORDER = "UpdateMenuItemOrder";
    String SOAP_ACTION_GET_UPDATE_ORDER = BASE_URL + "UpdateMenuItemOrder";

    String METHOD_GET_TODAY_SPECIAL = "GetTodaysSpecial";
    String SOAP_ACTION_GET_TODAY_SPECIAL = "http://app.cravrr.com/GetTodaysSpecial";

    String METHOD_GET_RESTAURANT_PROFILE = "GetMyProfile";
    String SOAP_ACTION_GET_RESTAURANT_PROFILE = BASE_URL + "GetMyProfile";

    String METHOD_GET_CUSTOMER_PROFILE = "GetCustomerProfile";
    String SOAP_ACTION_GET_CUSTOMER_PROFILE = BASE_URL + "GetCustomerProfile";

    String METHOD_GET_MY_BILL = "GetMyBill";
    String SOAP_ACTION_GET_MY_BILL = BASE_URL + "GetMyBill";

    String METHOD_SEND_NOTIFICATION = "SendNotification";
    String SOAP_ACTION_SEND_NOTIFICATION = BASE_URL + "SendNotification";


    int API_LOGIN = 1;
    int API_MENUITEM = 2;
    int API_CATEGORY = 3;
    int API_GET_TRANDING = 4;
    int API_REST_PROFILE = 5;
    int API_AUTH_STAFF = 6;

    int API_GET_CUSTOMER_PROFILE = 6;
    int API_GET_TABLE = 7;
    int API_GET_REGISTER = 8;
}
