package com.smokeysdinein.constatnts;

/**
 * all localy app constants define here
 * Created by Vineet.Rajpoot on 8/22/2015.
 */
public interface AppConstant {
    String TABLE_TOP_PREFS = "table_top_prefs";
    String EXTRA_FROM_MAIN = "extra_from_main";

//    int TODAYS_SPECIAL_ADAPTER =1;
//    int TRENDING_OFFER_ADAPTER =1;
//    int TODAYS_SPECIAL_ADAPTER =1;
//    int TODAYS_SPECIAL_ADAPTER =1;
//    int TODAYS_SPECIAL_ADAPTER =1;

    String PUSH_GET_WATER = "GET_WATER";
    String PUSH_GET_ASSISTANCE = "GET_ASSISTANCE";
    String PUSH_GET_BILL_CARD = "GET_MY_BILL_CARD";
    String PUSH_REFILL_ORDER = "REFILL_MY_ORDER";
    String PUSH_GET_BILL_CASH = "GET_MY_BILL_CASH";


    public String LOCAL_BROADCAST_CLEAR_ORDER = "local_broadcast_clear_order";
}
