package com.smokeysdinein.constatnts;

/**
 * Created by Monish on 25-08-2015.
 */
public interface ApiConstants {

    //String ITEM_IMAGE_URI = "http://testbuds.co.in/usrimgs/mnu_img/";
    //String CAT_IMAGE_URI = "http://testbuds.co.in/usrimgs/cat_img/";

    String ITEM_IMAGE_URI = "http://admin.cravrr.com/usrimgs/mnu_img/";
    String CAT_IMAGE_URI = "http://admin.cravrr.com/usrimgs/mnu_img/";

    interface PARAM {
        String USERNAME = "UserName";
        String PASSWORD = "Password";
        String REG_ID = "DeviceRegID";

    }


    public interface BOOKITEMORDERSPARAM {
        String CustomerID = "CustomerID";
        String CompBy = "CompBy";
        String ManagerCode = "ManagerCode";
        String ManagerPass = "ManagerPass";
        String CustomerName = "CustomerName";
        String Mobile = "Mobile";
        String EmailID = "EmailID";
        String DOB = "DOB";
        String Apartment = "Apartment";
        String AddressLine2 = "AddressLine2";
        String Street = "Street";
        String City = "City";
        String State = "State";
        String Country = "Country";
        String ZipCode = "ZipCode";
        String Address = "Address";
        String D_AddressLine1 = "D_AddressLine1";
        String D_AddressLine2 = "D_AddressLine2";
        String D_Apartment = "D_Apartment";
        String D_City = "D_City";
        String D_State = "D_State";
        String D_ZipCode = "D_ZipCode";
        String StaffID = "StaffID";
        String TableID = "TableID";
        String SpecialRequest = "SpecialRequest";
        String TipAmount = "TipAmount";
        String PhoneOrder = "PhoneOrder";
        String OrderType = "OrderType";
        String ResponseFormat = "ResponseFormat";
        String AddressToPrint = "AddressToPrint";
        String Extras = "Extras";
        String ComplementoryReason = "ComplementoryReason";
        String IsComplementory = "IsComplementory";
        String CategoryID = "CategoryID";
        String MenuItemID = "MenuItemID";
        String Quantity = "Quantity";
        String SpecialRemarks = "SpecialRemarks";
        String IngredientID = "IngredientID";
        String Price = "Price";
        String Name = "Name";
        String CouponID = "CouponID";
    }

}
