package com.smokeysdinein.model;

import java.util.ArrayList;

/**
 * Created by Vineet.Rajpoot on 8/25/2015.
 */
public class TrandingItem {
    public int CategoryID;
    public int MenuItemID;
    public float Price;
    public String ItemName;
    public String CategoryName;
    public String Description;
    public String ShortDescription;
    public String ItemImage;

    public boolean IsNonVeg;
    public boolean IsTrendingItem;
    public boolean IsTodaysSpecial;
    public boolean IsDrink;
    public boolean IsAllergic;
    public String Extras;
    public ArrayList<Ingredients> Ingredients;
}
