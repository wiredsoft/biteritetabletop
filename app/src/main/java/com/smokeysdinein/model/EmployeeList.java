package com.smokeysdinein.model;

import java.util.ArrayList;

/**
 * Created by Monish on 28-08-2015.
 */
public class EmployeeList {
    public int StaffID;
    public String StaffName;
    public String Mobile;
    public String EmailID;
    public String StaffCode;
    public String Address;
    public String City;
    public String Country;
    public String Photo;
    public String DOB;
    public String DOJ;
    public String Status;
    public String Category;
    ArrayList<Shifts> Shifts;
}
