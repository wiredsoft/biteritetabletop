package com.smokeysdinein.model;

import java.util.Comparator;

/**
 * Created by deepaksharma on 05/09/15.
 */
public class NameComparator implements Comparator<MenuItem> {
    @Override
    public int compare(MenuItem lhs, MenuItem rhs) {
        return lhs.ItemName.compareTo(rhs.ItemName);
    }
}
