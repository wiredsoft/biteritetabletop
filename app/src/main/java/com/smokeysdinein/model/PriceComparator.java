package com.smokeysdinein.model;

import java.util.Comparator;

/**
 * Created by deepaksharma on 05/09/15.
 */
public class PriceComparator implements Comparator<MenuItem> {

    @Override
    public int compare(MenuItem lhs, MenuItem rhs) {
        if (lhs.Price == rhs.Price)
            return 0;
        else if (lhs.Price > rhs.Price)
            return 1;
        else
            return -1;
    }
}
