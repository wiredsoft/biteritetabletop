package com.smokeysdinein.model;

/**
 * Created by Vineet.Rajpoot on 8/25/2015.
 */
public class IngredientModel {
    public int IngredientID;
    public int MenuItemID;
    public float Price;
    public String IngredientName;
}
