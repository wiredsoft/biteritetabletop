package com.smokeysdinein.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * menu item
 * Created by Monish on 23-08-2015.
 */

public class MenuItem implements Parcelable {
    public int CategoryID;
    public String CategoryName;
    public boolean IsDrink;
    public int MenuItemID;
    public String ItemName;
    public float Price;
    public String Calory;
    public String Description;
    public String ShortDescription;
    public String ItemImage;
    public boolean IsNonVeg;
    public boolean IsTrendingItem;
    public String ApproxCookingTime;
    public boolean IsTodaysSpecial;
    public List<Ingredients> Ingredients;
    public List<Taxes> Taxes;
    public List<SuggestiveItems> SuggestiveItems;
    public boolean IsAllergic;
    public String Extras;
    public String ItemCode;
    public int orderCount;
    public List<String>  addonsItem;
    public List<Integer>  addonsItemIndex;

    @Override
    public String toString() {
        return ItemName;
    }
    protected MenuItem(Parcel in) {
        CategoryID = in.readInt();
        CategoryName = in.readString();
        IsDrink = in.readByte() != 0x00;
        MenuItemID = in.readInt();
        ItemName = in.readString();
        Price = in.readFloat();
        Calory = in.readString();
        Description = in.readString();
        ShortDescription = in.readString();
        ItemImage = in.readString();
        IsNonVeg = in.readByte() != 0x00;
        IsTrendingItem = in.readByte() != 0x00;
        ApproxCookingTime = in.readString();
        IsTodaysSpecial = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            Ingredients = new ArrayList<Ingredients>();
            in.readList(Ingredients, Ingredients.class.getClassLoader());
        } else {
            Ingredients = null;
        }
        if (in.readByte() == 0x01) {
            Taxes = new ArrayList<Taxes>();
            in.readList(Taxes, Taxes.class.getClassLoader());
        } else {
            Taxes = null;
        }
        if (in.readByte() == 0x01) {
            SuggestiveItems = new ArrayList<SuggestiveItems>();
            in.readList(SuggestiveItems, SuggestiveItems.class.getClassLoader());
        } else {
            SuggestiveItems = null;
        }
        IsAllergic = in.readByte() != 0x00;
        Extras = in.readString();
        ItemCode = in.readString();
        orderCount = in.readInt();
        if (in.readByte() == 0x01) {
            addonsItem = new ArrayList<String>();
            in.readList(addonsItem, String.class.getClassLoader());
        } else {
            addonsItem = null;
        }
        if (in.readByte() == 0x01) {
            addonsItemIndex = new ArrayList<Integer>();
            in.readList(addonsItemIndex, Integer.class.getClassLoader());
        } else {
            addonsItemIndex = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(CategoryID);
        dest.writeString(CategoryName);
        dest.writeByte((byte) (IsDrink ? 0x01 : 0x00));
        dest.writeInt(MenuItemID);
        dest.writeString(ItemName);
        dest.writeFloat(Price);
        dest.writeString(Calory);
        dest.writeString(Description);
        dest.writeString(ShortDescription);
        dest.writeString(ItemImage);
        dest.writeByte((byte) (IsNonVeg ? 0x01 : 0x00));
        dest.writeByte((byte) (IsTrendingItem ? 0x01 : 0x00));
        dest.writeString(ApproxCookingTime);
        dest.writeByte((byte) (IsTodaysSpecial ? 0x01 : 0x00));
        if (Ingredients == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(Ingredients);
        }
        if (Taxes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(Taxes);
        }
        if (SuggestiveItems == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(SuggestiveItems);
        }
        dest.writeByte((byte) (IsAllergic ? 0x01 : 0x00));
        dest.writeString(Extras);
        dest.writeString(ItemCode);
        dest.writeInt(orderCount);
        if (addonsItem == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(addonsItem);
        }
        if (addonsItemIndex == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(addonsItemIndex);
        }
    }

    public static final Parcelable.Creator<MenuItem> CREATOR = new Parcelable.Creator<MenuItem>() {
        @Override
        public MenuItem createFromParcel(Parcel in) {
            return new MenuItem(in);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };


}
