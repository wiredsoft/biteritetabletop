package com.smokeysdinein.model;

/**
 * Created by Monish on 28-08-2015.
 */
public class FoodCategory {
    public int CategoryID;
    public String Category;
    public String Image;
    public String IsDrink;
    public boolean isSelected;
}
