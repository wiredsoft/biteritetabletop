package com.smokeysdinein.model;

/**
 * Created by Vineet.Rajpoot on 9/5/2015.
 */
public class GetMyProfileResult {
    public String Name;
    public String TagLine;
    public String Footer;
    public String Address;
    public String rest_image;
}
