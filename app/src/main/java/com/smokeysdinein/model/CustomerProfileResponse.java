package com.smokeysdinein.model;

/**
 * Created by Monish on 25-08-2015.
 */
public class CustomerProfileResponse {
    public int CustomerID;
    public String CustomerName;
    public String Appartment;
    public String Street;
    public String City;
    public String ZIPCode;
    public String Email;
    public String State;
    public String Country;
    public String DateOfBirth;
    public String Message;
    public String LastOrderDate;
    public String Address;
    public String AddressLine2;
    public String D_Appartment;
    public String D_AddressLine1;
    public String D_AddressLine2;
    public String D_City;
    public String D_ZIPCode;
    public String D_State;
    public boolean Status;

}
