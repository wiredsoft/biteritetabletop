package com.smokeysdinein.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Monish on 23-08-2015.
 */
public class Taxes implements Parcelable {
    public int TaxID;
    public String Head;
    public float Percentage;
    public float Surcharge;
    public float Ceas;

    protected Taxes(Parcel in) {
        TaxID = in.readInt();
        Head = in.readString();
        Percentage = in.readFloat();
        Surcharge = in.readFloat();
        Ceas = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(TaxID);
        dest.writeString(Head);
        dest.writeFloat(Percentage);
        dest.writeFloat(Surcharge);
        dest.writeFloat(Ceas);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Taxes> CREATOR = new Parcelable.Creator<Taxes>() {
        @Override
        public Taxes createFromParcel(Parcel in) {
            return new Taxes(in);
        }

        @Override
        public Taxes[] newArray(int size) {
            return new Taxes[size];
        }
    };
}
