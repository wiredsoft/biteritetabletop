package com.smokeysdinein.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Monish on 23-08-2015.
 */
public class Ingredients implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Ingredients> CREATOR = new Parcelable.Creator<Ingredients>() {
        @Override
        public Ingredients createFromParcel(Parcel in) {
            return new Ingredients(in);
        }

        @Override
        public Ingredients[] newArray(int size) {
            return new Ingredients[size];
        }
    };
    public int IngredientID;
    public int MenuItemID;
    public String IngredientName;
    public float Price;

    protected Ingredients(Parcel in) {
        IngredientID = in.readInt();
        MenuItemID = in.readInt();
        IngredientName = in.readString();
        Price = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(IngredientID);
        dest.writeInt(MenuItemID);
        dest.writeString(IngredientName);
        dest.writeFloat(Price);
    }
}
