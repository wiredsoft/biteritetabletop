package com.smokeysdinein.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by deepaksharma on 11/09/15.
 */
public class SuggestiveItems implements Parcelable {
    public int ItemID;
    public String ItemName;
    public String ItemImage;
    public String Calory;
    public String Price;

    protected SuggestiveItems(Parcel in) {
        ItemID = in.readInt();
        ItemName = in.readString();
        ItemImage = in.readString();
        Calory = in.readString();
        Price = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ItemID);
        dest.writeString(ItemName);
        dest.writeString(ItemImage);
        dest.writeString(Calory);
        dest.writeString(Price);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SuggestiveItems> CREATOR = new Parcelable.Creator<SuggestiveItems>() {
        @Override
        public SuggestiveItems createFromParcel(Parcel in) {
            return new SuggestiveItems(in);
        }

        @Override
        public SuggestiveItems[] newArray(int size) {
            return new SuggestiveItems[size];
        }
    };
}