/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smokeysdinein.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.smokeysdinein.R;
import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.constatnts.NetworkConstant;
import com.smokeysdinein.model.CommonModel;
import com.smokeysdinein.network.SoapRequest;
import com.smokeysdinein.network.UpdateViewListener;
import com.smokeysdinein.utils.NetworkUtils;
import com.smokeysdinein.utils.PreferenceUtil;

import java.io.IOException;
import java.util.HashMap;

public class RegistrationIntentService extends IntentService implements UpdateViewListener {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private Activity mActivity;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(token);

            // Subscribe to topic channels
            subscribeTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            sharedPreferences.edit().putBoolean(PreferenceUtil.SENT_TOKEN_TO_SERVER, true).apply();
            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(PreferenceUtil.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(PreferenceUtil.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        if (!NetworkUtils.isNetworkEnabled(this)) {

        }
        new SoapRequest(mActivity, NetworkConstant.METHOD_DIVICE_REG, NetworkConstant.SOAP_ACTION_DIVICE_REG, NetworkConstant.API_GET_REGISTER, this, "SecureAuthentication", CommonModel.class, getParams(), "REGID", token);
    }


    private HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(ApiConstants.PARAM.USERNAME, PreferenceUtil.getUserId());
        params.put(ApiConstants.PARAM.PASSWORD, PreferenceUtil.getPassword());
        return params;
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    @Override
    public void onUpdateView(boolean isSuccess, int apiType, Object response) {
        CommonModel model = (CommonModel) response;
        if (model != null && model.UtilResp != null && model.UtilResp.Code == 1) {
            PreferenceUtil.savePushStatus(true);
        } else {
            PreferenceUtil.savePushStatus(false);
        }
    }

    public void start(Activity activity, Intent intent) {
        mActivity = activity;
        //startService(intent);
    }
    // [END subscribe_topics]

}
