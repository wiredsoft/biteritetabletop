package com.smokeysdinein.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smokeysdinein.constatnts.ApiConstants;
import com.smokeysdinein.picasso.ImageTrans_RoundedCorner;
import com.squareup.picasso.Picasso;

import java.lang.ref.SoftReference;
import java.util.Hashtable;

/**
 * Created by vineet.rajpoot on 9/1/15.
 */
public class UiUtil {
    private static final Hashtable<String, SoftReference<Typeface>> mFontCache;
    private static String LOG_TAG = "UiUtil";

    static {
        mFontCache = new Hashtable<String, SoftReference<Typeface>>();
    }

    public static void expand(final View v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int) (targtetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density) + 200));
        v.startAnimation(a);
    }

    /**
     * method use to collapse a view smoothly
     *
     * @param v
     */
    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density) + 200));
        v.startAnimation(a);
    }

    public static void horizontalExpand(final View v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targteWidth = v.getMeasuredWidth();

        v.getLayoutParams().width = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().width = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int) (targteWidth * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(((int) (targteWidth / v.getContext().getResources().getDisplayMetrics().density) + 2000));
        v.startAnimation(a);
    }

    /**
     * method use to collapse a view smoothly
     *
     * @param v
     */
    public static void horizontalCollapse(final View v) {
        final int initialWidth = v.getMeasuredWidth();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().width = initialWidth - (int) (initialWidth * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(((int) (initialWidth / v.getContext().getResources().getDisplayMetrics().density) + 2000));
        v.startAnimation(a);
    }


    /**
     * @param pContext
     * @return
     */
    public static Typeface getFont(Context pContext, String pFontFilePathInAssets) {
        synchronized (mFontCache) {
            if (mFontCache.get(pFontFilePathInAssets) != null) {
                SoftReference<Typeface> ref = mFontCache.get(pFontFilePathInAssets);
                if (ref.get() != null) {
                    return ref.get();
                }
            }

            Typeface typeface = Typeface.createFromAsset(pContext.getAssets(), pFontFilePathInAssets);
            mFontCache.put(pFontFilePathInAssets, new SoftReference<Typeface>(typeface));
            return typeface;
        }
    }

    /**
     * @param pView
     * @param pContext
     * @param pAppliedAttrSet
     * @param pViewsAllAttrIdsArr
     * @param pFontAttributeId
     * @return true if font is applied, false otherwise
     */
    public static boolean setCustomFont(View pView, Context pContext, AttributeSet pAppliedAttrSet, int[] pViewsAllAttrIdsArr, int pFontAttributeId) {
        TypedArray typedArr = pContext.obtainStyledAttributes(pAppliedAttrSet, pViewsAllAttrIdsArr);
        String fontFileNameInAssets = typedArr.getString(pFontAttributeId);
        boolean isFontSet = setCustomFont(pView, pContext, fontFileNameInAssets);
        typedArr.recycle();
        return isFontSet;
    }

    /**
     * @param pView
     * @param pContext
     * @param pFontFileNameInAssets
     * @return
     */
    public static boolean setCustomFont(View pView, Context pContext, String pFontFileNameInAssets) {
        if (TextUtils.isEmpty(pFontFileNameInAssets)) {
            return false;
        }
        try {
            Typeface tf = getFont(pContext, pFontFileNameInAssets);
            if (pView instanceof TextView) {
                ((TextView) pView).setTypeface(tf);
            } else {
                ((Button) pView).setTypeface(tf);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * @param pSourceBmp
     * @param radius
     * @return
     */
    public static Bitmap getRoundedCircularBitmap(Bitmap pSourceBmp, int radius) {
        if (pSourceBmp == null || radius <= 0) {
            return pSourceBmp;
        }
        Bitmap outputBmp = null;

        try {

            outputBmp = Bitmap.createBitmap(pSourceBmp.getWidth(), pSourceBmp.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(outputBmp);

            int color = 0xffffffff;
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, pSourceBmp.getWidth(), pSourceBmp.getHeight());
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(rect.centerX(), rect.centerY(), radius, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(pSourceBmp, rect, rect, paint);
        } catch (Throwable tr) {
            Log.e(LOG_TAG, "Error in creating rounded bitmap", tr);
        }

        return outputBmp;
    }


    public static void loadImage(Context ctx, ImageView imageView, String uri) {
        Picasso.with(ctx).load(ApiConstants.ITEM_IMAGE_URI + uri).transform(new ImageTrans_RoundedCorner()).centerCrop().fit().into(imageView);
    }

}
