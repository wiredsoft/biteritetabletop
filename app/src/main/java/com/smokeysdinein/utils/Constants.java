package com.smokeysdinein.utils;

/**
 * Created by deepaksharma on 15/09/15.
 */
public class Constants {

    public static final String NOTIFY_ADAPTERS = "com.smokeydinen.notifyadapter";
    public static final String REFRESHUI = "com.smokeydinen.refreshui";
    public static final int GET_MY_BILL_CASH = 1;
    public static final int GET_WATER = 2;
    public static final int REFILL_MY_ORDER = 3;
    public static final int GET_MY_BILL_CARD = 4;
    public static final int GET_ASSISTANCE = 5;


}
