package com.smokeysdinein.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.smokeysdinein.application.SmokeyApplication;
import com.smokeysdinein.constatnts.AppConstant;
import com.smokeysdinein.ui.fragment.HomeFragment;

/**
 * prefrence used for store data
 * Created by vineet.rajpoot on 12/08/15.
 * Updated By Monish 23/8/2015
 */
public class PreferenceUtil {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String IS_PUSH_TOKEN_UPDATED = "push_token_updated";
    private static final String USER_ID = "userId";
    private static final String USER_PASSWORD = "password";
    private static final String MENU_ITEMS = "menu_items";
    private static final String FOOD_CATEGORIES = "food_categories";
    private static final String BOOKING_ID = "bookingid";
    private static final String USER_ORDERS = "userorders";
    private static final String TABLE_NUM = "table";
    private static final String TEMP_ID = "tempId";
    private static final String STAFF_CODE = "staff_code";
    private static final String STAFF_PASS = "staff_pass";
    private static final String INVOICE_NO = "invoice_no";

    private static final String CUSTUMER_ID = "custumerid";

    private static SharedPreferences mPreferences;
    private static PreferenceUtil mInstance;
    private static SharedPreferences.Editor mEditor;
    private static Context mContext;
    private static final String ORDERS = "order_place";
    private static String RES_IMAGE = "res_image";
    private static String RES_NAME = "res_name";


    private PreferenceUtil() {
    }

    public static PreferenceUtil getInstance() {
        if (mInstance == null) {
            mInstance = new PreferenceUtil();
            mContext = SmokeyApplication.mContext;
            mPreferences = mContext.getSharedPreferences(AppConstant.TABLE_TOP_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public static void setUserId(String userId) {
        mEditor.putString(USER_ID, userId).apply();
    }

    public static String getResImage() {
        return mPreferences.getString(RES_IMAGE, "");
    }

    public static void setResImage(String resImage) {
        mEditor.putString(RES_IMAGE, resImage).apply();
    }

    public static String getResName() {
        return mPreferences.getString(RES_NAME, "");
    }

    public static void setResName(String resName) {
        String a[] = resName.split(" ");
        if (a.length > 0) {
            mEditor.putString(RES_NAME, a[0]).apply();
        }
    }

    public static String getUserId() {
        return mPreferences.getString(USER_ID, "");
    }

    public static void setPassword(String userId) {
        mEditor.putString(USER_PASSWORD, userId).apply();
    }

    public static String getPassword() {
        return mPreferences.getString(USER_PASSWORD, "");
    }

    public static void saveMenuItems(String data) {
        mEditor.putString(MENU_ITEMS, data).apply();
    }

    public static String getMenuItems() {
        return mPreferences.getString(MENU_ITEMS, "");
    }

    public static void saveFoodCategories(String data) {
        mEditor.putString(FOOD_CATEGORIES, data).apply();
    }

    public static String getFoodCategories() {
        return mPreferences.getString(FOOD_CATEGORIES, "");
    }

    public static void saveBookingId(String bookingid) {
        mEditor.putString(BOOKING_ID, bookingid).apply();
    }

    public static String getStaffCode() {
        return mPreferences.getString(STAFF_CODE, null);
    }

    public static void saveStaffCode(String staffPass) {
        mEditor.putString(STAFF_CODE, staffPass).apply();
    }

    public static String getStaffPass() {
        return mPreferences.getString(STAFF_PASS, null);
    }

    public static void saveStaffPass(String staffId) {
        mEditor.putString(STAFF_PASS, staffId).apply();
    }

    public static String getBookingId() {
        return mPreferences.getString(BOOKING_ID, null);
    }

    public static void saveUserOrders(String bookingid) {
        mEditor.putString(USER_ORDERS, bookingid).apply();
    }

    public static String getUserOrders() {
        return mPreferences.getString(USER_ORDERS, null);
    }

    public static int getTableId() {
        return mPreferences.getInt(TABLE_NUM, 0);
    }

    public static void saveTableId(int id) {
        mEditor.putInt(TABLE_NUM, id).apply();
    }

    public static String getInvoiceNo() {
        return mPreferences.getString(INVOICE_NO, null);
    }

    public static void saveInvoiceNo(String invoiceno) {
        mEditor.putString(INVOICE_NO, invoiceno).apply();
    }

    public static void savePushStatus(boolean status) {
        mEditor.putBoolean(IS_PUSH_TOKEN_UPDATED, status);
    }


    public static boolean getPushStatus() {
        return mPreferences.getBoolean(IS_PUSH_TOKEN_UPDATED, false);
    }


    public static String getCustomerId() {
        return mPreferences.getString(CUSTUMER_ID, null);
    }

    public static void saveCustomerId(String custumerId) {
        mEditor.putString(CUSTUMER_ID, custumerId).apply();
    }


    public static void clearPrefrence() {
        mEditor.clear().commit();
    }

    public static String getTempId() {
        return mPreferences.getString(TEMP_ID, "");
    }

    public static void saveTempId(String id) {
        mEditor.putString(TEMP_ID, id).apply();
    }
}